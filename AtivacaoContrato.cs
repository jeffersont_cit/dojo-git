﻿using System;
using System.Collections.Generic;
using Mrv.Crm.Infra.Log;
using Mrv.Crm.Repositorio;
using Mrv.Crm.Util;
using Mrv.Crm.Util.CrmHelpers;
using Mrv.Crm.ValueObjects;
using Mrv.Crm.ValueObjects.Contrato;
using Mrv.Crm.WebReference.CrmService.Crm.Service;
using Status = Mrv.Crm.WebReference.CrmService.Crm.Service.Status;
using ValueObjectsProspect = Mrv.Crm.ValueObjects.Prospect;

namespace Mrv.Crm.Dominio
{
    /// <summary>
    /// Comentario adicionado - Silvio - teste
    /// </summary>
    public class AtivacaoContrato : IDisposable
    {
        #region [Propriedades]

        private Oportunidade _dominioOportunidadeVenda;
        private Oportunidade DominioOportunidadeVenda
        {
            get
            {
                return _dominioOportunidadeVenda ?? (_dominioOportunidadeVenda = new Oportunidade());
            }
        }

        private Contrato _dominioContrato;
        private Contrato DominioContrato
        {
            get
            {
                return _dominioContrato ?? (_dominioContrato = new Contrato());
            }
        }

        private PropostaVenda _dominioPropostaVenda;
        private PropostaVenda DominioPropostaVenda
        {
            get
            {
                return _dominioPropostaVenda ?? (_dominioPropostaVenda = new PropostaVenda());
            }
        }

        private PropostaAnaliseCredito _dominioPropostaAnaliseCredito;
        private PropostaAnaliseCredito DominioPropostaAnaliseCredito
        {
            get
            {
                return _dominioPropostaAnaliseCredito ?? (_dominioPropostaAnaliseCredito = new PropostaAnaliseCredito());
            }
        }

        private SystemUser _dominioUsuario;
        private SystemUser DominioUsuario
        {
            get
            {
                return _dominioUsuario ?? (_dominioUsuario = new SystemUser());
            }
        }

        private Unidade _dominioUnidade;
        private Unidade DominioUnidade
        {
            get
            {
                return _dominioUnidade ?? (_dominioUnidade = new Unidade());
            }
        }

        private ParametroDataBase _dominioParametroDataBase;
        private ParametroDataBase DominioParametroDataBase
        {
            get
            {
                return _dominioParametroDataBase ?? (_dominioParametroDataBase = new ParametroDataBase());
            }
        }

        private VendaGarantida _dominioVendaGarantida;
        private VendaGarantida DominioVendaGarantida
        {
            get
            {
                return _dominioVendaGarantida ?? (_dominioVendaGarantida = new VendaGarantida());
            }
        }

        private PlanoFinanciamento _dominioPlanoFinanciamento;
        private PlanoFinanciamento DominioPlanoFinanciamento
        {
            get
            {
                return _dominioPlanoFinanciamento ?? (_dominioPlanoFinanciamento = new PlanoFinanciamento());
            }
        }

        private RelacaoCliente _dominioRelacaoCliente;
        private RelacaoCliente DominioRelacaoCliente
        {
            get
            {
                return _dominioRelacaoCliente ?? (_dominioRelacaoCliente = new RelacaoCliente());
            }
        }

        private RepositorioPropostaAnaliseCredito _repositorioPropostaAnaliseCredito;
        private RepositorioPropostaAnaliseCredito RepositorioPropostaAnaliseCredito
        {
            get
            {
                return _repositorioPropostaAnaliseCredito ?? (_repositorioPropostaAnaliseCredito = new RepositorioPropostaAnaliseCredito());
            }
        }

        private RepositorioLoja _repositorioLoja;
        private RepositorioLoja RepositorioLoja
        {
            get
            {
                return _repositorioLoja ?? (_repositorioLoja = new RepositorioLoja());
            }
        }

        private RepositorioProposta _repositorioPropostaVenda;
        private RepositorioProposta RepositorioPropostaVenda
        {
            get
            {
                return _repositorioPropostaVenda ?? (_repositorioPropostaVenda = new RepositorioProposta());
            }
        }

        private RepositorioContrato _repositorioContrato;
        private RepositorioContrato RepositorioContrato
        {
            get
            {
                return _repositorioContrato ?? (_repositorioContrato = new RepositorioContrato());
            }
        }

        private RepositorioProdutodaProposta _repositorioProdutoProposta;
        private RepositorioProdutodaProposta RepositorioProdutoProposta
        {
            get
            {
                return _repositorioProdutoProposta ?? (_repositorioProdutoProposta = new RepositorioProdutodaProposta());
            }
        }

        private quote _propostaCrm;
        private opportunity _oportunidadeCrm;
        private new_propostaanalisecredito _propostaAnaliseCreditoCrm;

        private string[] _parametrosProposta = new[]
            {
                "new_valor_financiamento_sac",
                "new_valor_financiamento_price",
                "new_valor_fgts_pac",
                "new_status_solicitacao_pac",
                "new_financiamento",
                "new_valordofgts",
                "createdon",
                "new_valortotalproposta",
                "new_renda_presumida",
                "new_valor_subsidio",
                "new_renda_apurada_credito",
                "new_valor_indicacao_premiada"
            };

        public bool NaoPrecisaDeAnaliseDeCredito
        {
            get { return _propostaAnaliseCreditoCrm == null; }
        }

        /// <summary>
        /// Estrutura de mensagens.
        /// </summary>
        public struct Mensagem
        {
            public const string PROPOSTA_ANALISE_CREDITO_PLANO_FINACIAMENTO_INEXISTENTE =
                "Não existe plano de financiamento associado a oportunidade.";

            public const string OPORTUNIDADE_SICAQ_SEM_PAC_AGENDADA = "O contrato não pode ser pré-ativo, pois a PAC SICAQ não está agendada!";

            public const string ANALISE_CREDITO_NECESSARIA =
                "É necessário solicitar uma Análise de Crédito para a Oportunidade para pré-ativar o Contrato.";

            public const string DATA_INCLUSAO_LOJA_NULA =
                "Data de inclusão da Loja no módulo de crédito não pode ser nula. Favor entrar em contato com o Administrador do Sistema.";

            public const string MENSAGEM_COMPROMETIMENTO_INVALIDO =
                "Comprometimento de renda do cliente ultrapassou {0}  %. Para prosseguir com a venda, favor adicionar um fiador.";

            public const string VALIDACAO_STATUS_ATIVACAO_CONTRATO =
                "Contrato com status diferente do permitido. Status: {0}.";

            public const string VALIDACAO_OPORTUNIDADE_SEM_CONTRATO =
                "Oportunidade sem contrato.";

            public const string VALIDACAO_PERMISSAO_ATIVACAO_CONTRATO =
                "Usuário sem permissão para {0} Contrato.";

            public const string VALIDACAO_CAMPOS_ATIVACAO_CONTRATO =
                "O contrato possui campos em branco. Campos necessários: Testemunha 1, Testemunha 2, CPF 1, CPF 2 e Data Base.";

            public const string COTA_UNIDADE_CSG_ATINGIDA =
                "Cota de unidade CSG atingida para essa data base, a reserva não pode ser criada.";

            public const string MENSAGEM_COTA_NAO_DEFINIDA =
                "Quantidade de Cotas CSG não foi definida para essa Data Base.";

            public const string MENSAGEM_VALORES_FINANCIAMENTO_CONTIDO_PROPOSTA_MENOR_PAC =
                "Os valores de financiamento contidos na PAC no momento do ganho da proposta (SAC {0} e PRICE {1}) estão menores que os atuais valores aprovados na PAC (SAC {2} e PRICE {3}). Para pré ativar o contrato será necessário contestar a PAC ou ajustar o valor de financiamento informado por meio do processo de cancelamento do contrato e revisão da proposta.";

            public const string MENSAGEM_VALOR_FGTS_CONTIDO_PROPOSTA_MENOR_PAC =
                "O valor de FGTS contido na PAC no momento do ganho da proposta ({0}) esta menor que o atual valor aprovado na PAC ({1}). Para pré ativar o contrato será necessário contestar a PAC ou ajustar o valor de financiamento informado por meio do processo de cancelamento do contrato e revisão da proposta.";

            public const string MENSAGEM_VALORES_FINANCIAMENTO_CONTIDO_PROPOSTA_MAIOR_PAC =
                "O valor de financiamento informado na proposta ({0}) é maior que o valor aprovado na PAC ({1} + {2}). Para pré ativar o contrato será necessário contestar a PAC ou ajustar o valor de financiamento informado por meio do processo de cancelamento do contrato e revisão da proposta.";

            public const string MENSAGEM_VALOR_FGTS_CONTIDO_PROPOSTA_MAIOR_PAC =
                "O valor de FGTS informado na proposta ({0}) é maior que o valor aprovado na PAC ({1}). Para pré-ativar o contrato será necessário contestar a PAC ou ajustar o valor de FGTS informado por meio do processo de cancelamento do contrato e revisão da proposta.";

            public const string MENSAGEM_ALCADA_MINIMO =
                "O percentual atual da regra de {0} mínimo {1} difere do percentual permitido para a alçada {2}. Para pré-ativar o contrato será necessário contestar a PAC ou ajustar o valor de {0} informado por meio do processo de cancelamento do contrato e revisão da proposta.";
            public const string ERRO_VALIDAR_REGRAS_DEFINICAO_ALCADA = "Erro ao validar dados definição de alcada no Mrv Comercial,  contate o adiministrador do sistemas.";
            public const string ERRO_VALIDAR_ATIVACAO_CONTRATO = "Erro ao validar pré-ativação / ativação do contrato, contate o adiministrador do sistemas.";
            public const string FGTS = "FGTS";
            public const string FINANCIAMENTO = "financiamento";

            public const string VALOR_SUBSIDIO_PROPOSTA_MENOR_VALOR_SUBSIDIO_PAC = "O valor de subsídio contido na PAC no momento do ganho da proposta ({0}) esta menor que o atual valor aprovado na PAC ({1}). Para pré ativar o contrato será necessário contestar a PAC ou ajustar o valor de subsídio informado por meio do processo de cancelamento do contrato e revisão da proposta.";

            public const string PAC_RENDA_APURADA_ALTERADA = "Valor da Renda Apurada pelo Crédito foi alterada para {0}, Para pré ativar o contrato será necessário contestar a PAC ou ajustar o valor Renda Presumida informado por meio do processo de cancelamento do contrato e revisão da proposta.";

            public const string MENSAGEM_INDICACAO_PREMIADA = "Já existe uma oportunidade com contrato pré-ativo com o desconto de indicação utilizado, para prosseguir com a venda favor revisar a proposta.";
        };

        #endregion

        /// <summary>
        /// Método para validar a pré-ativação do contrato.
        /// </summary>
        /// <param name="oportunidadeId"></param>
        /// <returns></returns>
        public List<RetornoPreAtivacaoContrato> ValidarAtivacaoContrato(Guid oportunidadeId)
        {
            var retornoValidacao = new List<RetornoPreAtivacaoContrato>();
            try
            {
                retornoValidacao = ValidaPreAtivacaoContratoComAnaliseCredito(oportunidadeId);

                retornoValidacao.AddRange(ValidarInformacoesContrato(oportunidadeId));

                retornoValidacao.AddRange(ValidarRegrasIndicacaoPremiadaPreAtivarContrato(oportunidadeId));
            }
            catch (Exception ex)
            {
                var log = new Infra.Log.LogIntegracao
                {
                    MensagemEntrada = string.Format("Oportunidade ID: {0}", oportunidadeId),
                    Status = Infra.Log.Status.ERRO,
                    SistemaDestino = Sistema.CRM,
                    SistemaOrigem = Sistema.CRM,
                    Origem = ValueObjectsProspect.Constantes.VALIDAR_REGRAS_ATIVACAO_CONTRATO,
                    NomeEntidade = EntityName.opportunity.ToString(),
                    IdRegistroRelacionado = oportunidadeId,
                    MensagemRetorno = Utility.ObterDescricaoErro(ex)
                };
                LogManager.Criar(log);

                retornoValidacao.Add(new RetornoPreAtivacaoContrato(TipoGrupoValidacao.ErroGenerico,
                    TipoErroPreAtivacaoContrato.ErroGenerico, Mensagem.ERRO_VALIDAR_ATIVACAO_CONTRATO, false));
            }

            return retornoValidacao;
        }

        public string ValidarPermissaoAtivacaoContrato(Guid oportunidadeId)
        {
            var isPreAtivacao = DominioUsuario.UsuarioPertenceA(new[] { Equipe.NomeEquipe.PRE_ATIVACAO });
            var isAtivacao = DominioUsuario.UsuarioPertenceA(new[] { Equipe.NomeEquipe.ATIVACAO });
            if (_oportunidadeCrm == null)
            {
                _oportunidadeCrm = (opportunity)DominioOportunidadeVenda.ObterPorId(oportunidadeId);
            }

            var contrato = DominioContrato.ObterPorOportunidadeInformada(oportunidadeId, "billingendon", "expireson",
                "customerid", "serviceaddress", "billtoaddress", "statuscode", "new_testemunha1", "new_testemunha2",
                "new_cpf1", "new_cpf2", "new_data_base", "new_tipo_contrato");
            var statusCode = Contrato.StatusContrato.RASCUNHO;
            if (contrato != null && contrato.statuscode != null)
            {
                statusCode = contrato.statuscode.Value;
            }
            if (!isPreAtivacao || !isAtivacao)
            {
                // Formata a mensagem de validação de permissão de ativação de contrato
                var mensagemRetorno = string.Format(Mensagem.VALIDACAO_PERMISSAO_ATIVACAO_CONTRATO,
                    statusCode == Contrato.StatusContrato.RASCUNHO ? "Pré-Ativar" : "Ativar");

                if (!isAtivacao && statusCode == Contrato.StatusContrato.RASCUNHO_PRE_ATIVO)
                {
                    return mensagemRetorno;
                }
                if (!isPreAtivacao && statusCode == Contrato.StatusContrato.RASCUNHO)
                {
                    if (_oportunidadeCrm.new_tipodeoportunidade.Value == Entidade.Oportunidade.TipoDeOportunidade.VENDA_UNIDADE ||
                        _oportunidadeCrm.new_tipodeoportunidade.Value == Entidade.Oportunidade.TipoDeOportunidade.VENDA_GARAGEM)
                    {
                        return mensagemRetorno;

                    }
                    if (!isAtivacao)
                    {
                        return mensagemRetorno;
                    }
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Método para validar a pré-ativação do contrato.
        /// </summary>
        /// <param name="oportunidadeId"></param>
        /// <returns></returns>
        private List<RetornoPreAtivacaoContrato> ValidaPreAtivacaoContratoComAnaliseCredito(Guid oportunidadeId)
        {
            var listaRetorno = new List<RetornoPreAtivacaoContrato>();
            _oportunidadeCrm = (opportunity)DominioOportunidadeVenda.ObterPorId(oportunidadeId);
            var contratoCrm = DominioContrato.ObterPorOportunidade(oportunidadeId, _oportunidadeCrm.GetType());
            _propostaCrm = DominioPropostaVenda.ObterPropostaGanhaPorOportunidade(oportunidadeId, _parametrosProposta);

            //verifica se a oportunidade não foi criada pelo MRV Corretor.
            //Troca da verificação de new_corretoroportunidadeid por new_loja_criacaoid
            if (_oportunidadeCrm.new_loja_criacaoid == null)
            {
                return listaRetorno;
            }

            RepositorioPropostaAnaliseCredito.UseAllFields = true;
            _propostaAnaliseCreditoCrm = RepositorioPropostaAnaliseCredito.ObterPorOportunidade(oportunidadeId);

            //Caso não exista PAC o sistema verificará se a loja do corretor utiliza o módulo do MRV Crédito,
            //Se utilizar sitema informa que será ncessário solicitar uma PAC para pré ativar o contrato.
            if (_propostaAnaliseCreditoCrm == null)
            {
                //Verificar se o Plano de financimento da Oportunidade
                //exige análise de crédito
                var planoFinanciamento =
                    new PlanoFinanciamento().ObterPorOportunidade(oportunidadeId);
                if (planoFinanciamento == null)
                {
                    listaRetorno.Add(new RetornoPreAtivacaoContrato(TipoGrupoValidacao.NaoAprovado,
                        TipoErroPreAtivacaoContrato.PlanoFinaciamentoInexistente,
                        Mensagem.PROPOSTA_ANALISE_CREDITO_PLANO_FINACIAMENTO_INEXISTENTE, false));
                }
                else
                {
                    //Valida se o plano de finaciamento exige uma análise de crédito
                    if (!ValidarPlanoFlexBeneficioIss(oportunidadeId, planoFinanciamento))
                    {
                        if (planoFinanciamento.new_solicita_analise_credito == null ||
                        planoFinanciamento.new_solicita_analise_credito.Value == false)
                        {
                            return listaRetorno;
                        }
                    }
                }

                if (_oportunidadeCrm.new_loja_criacaoid != null)
                {
                    //Verifica se deve exigir uma PAC para oportunidade
                    var mensagemLojaHabilitadaModuloCredito = ValidaLojaCorretorUtilizaModuloCredito(_oportunidadeCrm.new_loja_criacaoid.Value,
                        _propostaCrm);
                    if (!string.IsNullOrEmpty(mensagemLojaHabilitadaModuloCredito))
                    {
                        listaRetorno.Add(new RetornoPreAtivacaoContrato(TipoGrupoValidacao.NaoAprovado,
                            TipoErroPreAtivacaoContrato.LojaHabilitadaModuloCredito,
                            mensagemLojaHabilitadaModuloCredito, false));
                    }
                }
            }

            if (contratoCrm != null && contratoCrm.contractid != null)
            {
                //contrato em rascunho
                if (contratoCrm.statuscode.Value == Entidade.Contrato.StatusCode.RASCUNHO)
                {
                    //Verificar se existe solicitação de análise de crédito.
                    if (_propostaAnaliseCreditoCrm != null)
                    {
                        //valida se é análise de crédito.
                        if (_propostaAnaliseCreditoCrm.new_propostaanalisecreditoid != null)
                        {
                            if (_propostaAnaliseCreditoCrm.new_tiposolicitacao.Value ==
                                (int)Entidade.PropostaAnaliseCredito.TipoSolicitacao.ANALISE_DE_CREDITO)
                            {
                                #region FA03 – Pré-ativação de contrato

                                //FA03 – Pré-ativação de contrato
                                //•	Verificar se existe solicitação de análise de crédito.
                                //•	Se existe solicitação e retorno ainda não recebido,
                                //exibe a mensagem “Analise de crédito em andamento! Aguarde o retorno do crédito para pré-ativar o contrato”.
                                if (_oportunidadeCrm.new_elegibilidade != null && _oportunidadeCrm.new_elegibilidade.Value == Entidade.Oportunidade.Elegibilidade.SICAQ)
                                {
                                    if (_propostaAnaliseCreditoCrm.new_statusdoagendamento == null ||
                                        (_propostaAnaliseCreditoCrm.new_statusdoagendamento.Value != Entidade.PropostaAnaliseCredito.StatusAgendamentoPAC.Agendado &&
                                        _propostaAnaliseCreditoCrm.new_statusdoagendamento.Value != Entidade.PropostaAnaliseCredito.StatusAgendamentoPAC.FormularioAssinado))
                                    {
                                        listaRetorno.Add(new RetornoPreAtivacaoContrato(TipoGrupoValidacao.NaoAprovado,
                                            TipoErroPreAtivacaoContrato.OportunidadeSicaqSemPacAgendada,
                                            Mensagem.OPORTUNIDADE_SICAQ_SEM_PAC_AGENDADA, false));
                                    }
                                }
                                if (_propostaAnaliseCreditoCrm.new_statusdaanalise != null)
                                {
                                    var mensagemStatusPacInvalido = DominioPropostaAnaliseCredito.ValidarStatusPAC(
                                        _propostaAnaliseCreditoCrm.new_statusdaanalise.Value,
                                        Utility.MomentoValidacaoPAC.AtivarContrato);
                                    if (!string.IsNullOrEmpty(mensagemStatusPacInvalido))
                                    {
                                        listaRetorno.Add(new RetornoPreAtivacaoContrato(TipoGrupoValidacao.NaoAprovado,
                                            TipoErroPreAtivacaoContrato.StatusPacInvalido, mensagemStatusPacInvalido, false));
                                    }
                                }
                                #endregion
                            }
                        }


                    }
                }
            }
            return listaRetorno;
        }

        /// <summary>
        ///Método responsável por verificar se a loja do Corretor utiliza o módulo do crédito,
        ///Se utilizar, informa que é obrigatório a solicitação de uma PAC para prosseguir
        /// </summary>
        /// <returns></returns>
        private string ValidaLojaCorretorUtilizaModuloCredito(Guid lojaid, quote propostaCrm)
        {
            RepositorioLoja.UseAllFields = true;
            var lojaCrm = RepositorioLoja.Obter(lojaid);

            if (lojaCrm != null)
            {
                //verifica se a loja esta habilitada a usar o módulo de
                //crédito
                if (lojaCrm.new_usamrvcredito != null && lojaCrm.new_usamrvcredito.Value)
                {

                    if (lojaCrm.new_data_inclusao_credito != null)
                    {
                        //verifica se a criação da proposta é maior ou igual a data de inclusão
                        //e a loja esta habilitada, caso seja verdadeira será necessário a criação da pac.
                        if (propostaCrm != null && propostaCrm.createdon != null &&
                            Convert.ToDateTime(propostaCrm.createdon.Value) >=
                            Convert.ToDateTime(lojaCrm.new_data_inclusao_credito.Value) &&
                            lojaCrm.new_usamrvcredito.Value)
                        {
                            return Mensagem.ANALISE_CREDITO_NECESSARIA;
                        }
                    }
                    else
                    {
                        return Mensagem.DATA_INCLUSAO_LOJA_NULA;
                    }
                }
            }
            return string.Empty;
        }

        public contract ObterContrato(Guid oportunidadeId)
        {
            return DominioContrato.ObterPorOportunidadeInformada(oportunidadeId, "billingendon", "expireson",
                "customerid", "serviceaddress", "billtoaddress", "statuscode", "new_testemunha1", "new_testemunha2",
                "new_cpf1", "new_cpf2", "new_data_base", "new_tipo_contrato", "new_opportunityid");
        }

        private IEnumerable<RetornoPreAtivacaoContrato> ValidarInformacoesContrato(Guid oportunidadeId)
        {
            var classificacaoPlanoFinanciamento = 0;

            if (oportunidadeId != null)
            {
                var planoFinanciamento = DominioPlanoFinanciamento.ObterPorOportunidade(oportunidadeId);
                if (planoFinanciamento != null && planoFinanciamento.new_classificacao != null)
                {
                    classificacaoPlanoFinanciamento = planoFinanciamento.new_classificacao.Value;
                }
            }

            var isPlanoFlex = Entidade.PlanoDeFinanciamento.Classificacao.FLEX == classificacaoPlanoFinanciamento;

            var retornoValidacao = new List<RetornoPreAtivacaoContrato>();
            if (_oportunidadeCrm == null)
            {
                _oportunidadeCrm = (opportunity)DominioOportunidadeVenda.ObterPorId(oportunidadeId);
            }

            var contrato = ObterContrato(oportunidadeId);
            if (contrato == null)
            {
                retornoValidacao.Add(new RetornoPreAtivacaoContrato(TipoGrupoValidacao.InformacoesContrato,
                    TipoErroPreAtivacaoContrato.OportunidadeSemContrato, Mensagem.VALIDACAO_OPORTUNIDADE_SEM_CONTRATO, false));
            }
            else
            {
                var statusCode = Contrato.StatusContrato.RASCUNHO;
                if (contrato.statuscode != null)
                {
                    statusCode = contrato.statuscode.Value;
                }

                //Verifica campos se estão preenchidos para ativar ou pré-ativar.
                if ((contrato.new_tipo_contrato.Value != (int)Contrato.TipoContrato.Renegociacao &&
                     contrato.new_tipo_contrato.Value != (int)Contrato.TipoContrato.VendaPermuta) &&
                    (contrato.new_testemunha1 == null
                     || contrato.new_testemunha2 == null || contrato.new_cpf1 == null
                     || contrato.new_cpf2 == null || contrato.new_data_base == null))
                {
                    retornoValidacao.Add(new RetornoPreAtivacaoContrato(TipoGrupoValidacao.InformacoesContrato,
                        TipoErroPreAtivacaoContrato.CamposContratoNaoPreenchido, Mensagem.VALIDACAO_CAMPOS_ATIVACAO_CONTRATO,
                        false));
                }

                if (statusCode == Contrato.StatusContrato.RASCUNHO)
                {
                    var mensagemApuracao = ValidarRegrasApuracaoRenda(oportunidadeId, contrato.contractid.Value);
                    if (!string.IsNullOrEmpty(mensagemApuracao))
                    {
                        retornoValidacao.Add(new RetornoPreAtivacaoContrato(TipoGrupoValidacao.InformacoesContrato,
                        TipoErroPreAtivacaoContrato.ComprometimentoInvalido, mensagemApuracao,
                        true));
                    }

                    //Verifica Cota CSG e unidades já vendiadas.
                    if (contrato.new_data_base != null)
                    {
                        try
                        {
                            var dataBaseContrato = Convert.ToDateTime(contrato.new_data_base.Value);
                            var produto = DominioUnidade.ObterPorOportunidade(oportunidadeId, new[] { "productid" });
                            var existemUnidadesCsgDisponiveis =
                                DominioParametroDataBase.ValidarReservaCotaCSGPorProduto(produto.productid.Value,
                                    dataBaseContrato);
                            if (!existemUnidadesCsgDisponiveis)
                            {
                                retornoValidacao.Add(
                                    new RetornoPreAtivacaoContrato(TipoGrupoValidacao.InformacoesContrato,
                                        TipoErroPreAtivacaoContrato.CotaUnidadeCsgAtingida,
                                        Mensagem.COTA_UNIDADE_CSG_ATINGIDA,
                                        false));
                            }
                        }
                        catch (Exception)
                        {
                            retornoValidacao.Add(
                                      new RetornoPreAtivacaoContrato(TipoGrupoValidacao.InformacoesContrato,
                                          TipoErroPreAtivacaoContrato.CotaUnidadeCsgAtingida,
                                          Mensagem.MENSAGEM_COTA_NAO_DEFINIDA,
                                          false));
                        }

                    }
                    if (!isPlanoFlex)
                    {


                        if (_oportunidadeCrm.new_tipodeoportunidade != null &&
                            _oportunidadeCrm.new_tipodeoportunidade.Value ==
                            Entidade.Oportunidade.TipoDeOportunidade.VENDA_UNIDADE)
                        {
                            var listaValidacoesAlcada = ValidarRegrasDefinicaoAlcada(retornoValidacao, _propostaCrm, oportunidadeId);

                            ValidacoesFinanciamentoFgtsMinimo(retornoValidacao, _propostaAnaliseCreditoCrm, _propostaCrm, listaValidacoesAlcada);
                        }

                        var mensagemValidacaoRenda = DominioPropostaVenda.ValidarRendaPropostaRendaPac(_propostaCrm, _propostaAnaliseCreditoCrm, Utility.MomentoValidacaoPAC.AtivarContrato);
                        if (!string.IsNullOrEmpty(mensagemValidacaoRenda))
                        {
                            retornoValidacao.Add(
                                          new RetornoPreAtivacaoContrato(TipoGrupoValidacao.NaoAprovado,
                                              TipoErroPreAtivacaoContrato.Renda,
                                              mensagemValidacaoRenda,
                                              true));
                        }

                        var mensagemValidacaoEnquadramentoPac =
                            DominioPropostaVenda.ValidarRegraEnquadramentoPAC(_propostaAnaliseCreditoCrm, _propostaCrm,
                                _oportunidadeCrm, Utility.MomentoValidacaoPAC.AtivarContrato);
                        if (!string.IsNullOrEmpty(mensagemValidacaoEnquadramentoPac))
                        {
                            retornoValidacao.Add(
                                          new RetornoPreAtivacaoContrato(TipoGrupoValidacao.InformacoesContrato,
                                              TipoErroPreAtivacaoContrato.ValidacaoEnquadramentoPac,
                                              mensagemValidacaoEnquadramentoPac,
                                              true));

                        }
                    }
                }
                if (statusCode != Contrato.StatusContrato.RASCUNHO &&
                    statusCode != Contrato.StatusContrato.ATIVO &&
                    statusCode != Contrato.StatusContrato.RASCUNHO_PRE_ATIVO)
                {
                    var mensagemRetorno = string.Format(Mensagem.VALIDACAO_STATUS_ATIVACAO_CONTRATO,
                        contrato.statuscode != null ? contrato.statuscode.name : string.Empty);
                    retornoValidacao.Add(new RetornoPreAtivacaoContrato(TipoGrupoValidacao.InformacoesContrato,
                        TipoErroPreAtivacaoContrato.StatusContratoInvalido, mensagemRetorno, false));
                }


            }

            return retornoValidacao;
        }

        private List<RetornoRegrasAlcada> ValidarRegrasDefinicaoAlcada(List<RetornoPreAtivacaoContrato> retornoValidacao, quote proposta, Guid oportunidadeId)
        {
            if (DominioPlanoFinanciamento.VerificarPlanoAssociativoBancario(oportunidadeId))
            {

                if (proposta != null && proposta.quoteid != null)
                {
                    var retornoDefinicaoAlcada = RepositorioPropostaVenda.ValidarRegrasAlcada(proposta.quoteid.Value.ToString());


                    if (retornoDefinicaoAlcada != null && retornoDefinicaoAlcada.Count > 0)
                    {
                        foreach (var item in retornoDefinicaoAlcada)
                        {
                            if (item.IndicadorErro == RetornoRegrasAlcada.ValorIndicadorErro.Erro)
                            {
                                retornoValidacao.Add(new RetornoPreAtivacaoContrato(TipoGrupoValidacao.ErroGenerico,
                                                         TipoErroPreAtivacaoContrato.PercentualFgtsFinanciamentoMinimo,
                                                         Mensagem.ERRO_VALIDAR_REGRAS_DEFINICAO_ALCADA,
                                                         false));
                            }
                            else
                            {
                                if (item.StatusRegra == RetornoRegrasAlcada.ValorStatusRegra.INVALIDA)
                                {
                                    var nomeRegra = item.IdTipoRegra == RetornoRegrasAlcada.ValorTipoRegra.FGTS_MINIMO ? Mensagem.FGTS : Mensagem.FINANCIAMENTO;

                                    var memsagem = string.Format(Mensagem.MENSAGEM_ALCADA_MINIMO, nomeRegra, item.ParametroRegraCalculado, item.ParametroRegraPermitido);

                                    retornoValidacao.Add(new RetornoPreAtivacaoContrato(TipoGrupoValidacao.NaoAprovado,
                                                         TipoErroPreAtivacaoContrato.PercentualFgtsFinanciamentoMinimo,
                                                         memsagem,
                                                         true));
                                }
                            }
                        }
                    }
                    return retornoDefinicaoAlcada;
                }
            }
            return null;
        }

        private void ValidacoesFinanciamentoFgtsMinimo(List<RetornoPreAtivacaoContrato> retornoValidacao, new_propostaanalisecredito propostaAnaliseCreditoCrm, quote proposta, List<RetornoRegrasAlcada> regrasAlcada)
        {
            if (propostaAnaliseCreditoCrm != null && proposta != null && regrasAlcada != null && regrasAlcada.Count > 0)
            {
                foreach (var item in regrasAlcada)
                {
                    if (item != null && item.IdTipoRegra != 0)
                    {
                        var parametroCalculado = item.ParametroRegraCalculado != null ? Convert.ToDouble(item.ParametroRegraCalculado.Replace("%", string.Empty).Replace(".", ",")) : 0;
                        var parametroCalculadoGanho = item.ParametroRegraCalculadoGanho != null ? Convert.ToDouble(item.ParametroRegraCalculadoGanho.Replace("%", string.Empty).Replace(".", ",")) : 0;
                        if (RetornoRegrasAlcada.ValorTipoRegra.FGTS_MINIMO == item.IdTipoRegra && (item.ParametroRegraCalculadoGanho == null || parametroCalculado < parametroCalculadoGanho))
                        {
                            ValidarValorFgtsPropostaMenorValorFgtsProposta(retornoValidacao, propostaAnaliseCreditoCrm, proposta);
                        }
                        if (RetornoRegrasAlcada.ValorTipoRegra.FINANCIAMENTO_MINIMO == item.IdTipoRegra && (item.ParametroRegraCalculadoGanho == null || parametroCalculado < parametroCalculadoGanho))
                        {
                            ValidarValorFinanciamentoPropostaMenorValorFinanciamentoPac(retornoValidacao, propostaAnaliseCreditoCrm, proposta);
                            ValidarValorSubsidioPropostaMenorPac(retornoValidacao, propostaAnaliseCreditoCrm, proposta);
                        }
                    }
                }
                ValidarValorFinanciamentoPropostaMaiorQueMaiorValorFinanciamentoPac(retornoValidacao, propostaAnaliseCreditoCrm, proposta);
                ValidarValorFgtsPropostaMaiorValorFgtsPac(retornoValidacao, propostaAnaliseCreditoCrm, proposta);

            }
        }

        private void ValidarValorFgtsPropostaMaiorValorFgtsPac(ICollection<RetornoPreAtivacaoContrato> retornoValidacao, new_propostaanalisecredito propostaAnaliseCreditoCrm, quote proposta)
        {
            if (propostaAnaliseCreditoCrm != null && proposta != null)
            {
                if (proposta.new_valordofgts == null)
                {
                    proposta.new_valordofgts = new CrmMoney { Value = 0 };
                }
                var valorFgtsProposta = proposta.new_valordofgts.Value;

                if (propostaAnaliseCreditoCrm.new_valorfgts == null)
                {
                    propostaAnaliseCreditoCrm.new_valorfgts = new CrmDecimal { Value = 0 };
                }
                var valorFgtsPropostaAnaliseCredito = propostaAnaliseCreditoCrm.new_valorfgts.Value;

                var retornoValidarValoresFgtsPropostaMaiorPac =
                    DominioPropostaVenda.ValidarValoresFGTSPropostaMaiorPac(propostaAnaliseCreditoCrm, proposta);
                if (!string.IsNullOrEmpty(retornoValidarValoresFgtsPropostaMaiorPac))
                {
                    var mensagemValidacaoValoresFgtsPropostaMaiorPac =
                        string.Format(Mensagem.MENSAGEM_VALOR_FGTS_CONTIDO_PROPOSTA_MAIOR_PAC,
                            Utility.formataMoeda(valorFgtsProposta.ToString()),
                            Utility.formataMoeda(valorFgtsPropostaAnaliseCredito.ToString()));

                    retornoValidacao.Add(new RetornoPreAtivacaoContrato(TipoGrupoValidacao.ValoresDaPropostaSuperiores,
                        TipoErroPreAtivacaoContrato.ValoresFinanciamentoPacProposta,
                        mensagemValidacaoValoresFgtsPropostaMaiorPac, true));
                }
            }
        }

        private void ValidarValorFinanciamentoPropostaMaiorQueMaiorValorFinanciamentoPac(ICollection<RetornoPreAtivacaoContrato> retornoValidacao, new_propostaanalisecredito propostaAnaliseCreditoCrm, quote proposta)
        {
            if (propostaAnaliseCreditoCrm != null && proposta != null)
            {
                if (propostaAnaliseCreditoCrm.new_valorfinanciamento == null)
                {
                    propostaAnaliseCreditoCrm.new_valorfinanciamento = new CrmDecimal { Value = 0 };
                }
                var valorFinanciamentoSacPropostaAnaliseCredito = propostaAnaliseCreditoCrm.new_valorfinanciamento.Value;

                if (propostaAnaliseCreditoCrm.new_valor_financiamento_price == null)
                {
                    propostaAnaliseCreditoCrm.new_valor_financiamento_price = new CrmDecimal { Value = 0 };
                }
                var valorFinanciamentoPricePropostaAnaliseCredito =
                    propostaAnaliseCreditoCrm.new_valor_financiamento_price.Value;
                if (proposta.new_financiamento == null)
                {
                    proposta.new_financiamento = new CrmFloat { Value = 0 };
                }
                var valorFinanciamentoProposta = proposta.new_financiamento.Value;

                if (propostaAnaliseCreditoCrm.new_valorsubsidio == null)
                {
                    propostaAnaliseCreditoCrm.new_valorsubsidio = new CrmDecimal { Value = 0 };
                }
                var valorSubsidio = propostaAnaliseCreditoCrm.new_valorsubsidio.Value;


                var maiorValor = valorFinanciamentoSacPropostaAnaliseCredito;

                if (maiorValor < valorFinanciamentoPricePropostaAnaliseCredito)
                {
                    maiorValor = valorFinanciamentoPricePropostaAnaliseCredito;
                }

                maiorValor = maiorValor + valorSubsidio;
                var retornoValidarValoresFinanciamentoPropostaMaiorPac =
                    DominioPropostaVenda.ValidarValoresFinanciamentoPropostaMaiorPac(propostaAnaliseCreditoCrm, proposta, Utility.MomentoValidacaoPAC.AtivarContrato);
                if (!string.IsNullOrEmpty(retornoValidarValoresFinanciamentoPropostaMaiorPac))
                {
                    var mensagemValidacaoValoresFinanciamentoPropostaMaiorPac =
                        string.Format(Mensagem.MENSAGEM_VALORES_FINANCIAMENTO_CONTIDO_PROPOSTA_MAIOR_PAC,
                            Utility.formataMoeda(valorFinanciamentoProposta.ToString()),
                            Utility.formataMoeda(maiorValor.ToString()),
                            Utility.formataMoeda(valorSubsidio.ToString()));

                    retornoValidacao.Add(new RetornoPreAtivacaoContrato(TipoGrupoValidacao.ValoresDaPropostaSuperiores,
                        TipoErroPreAtivacaoContrato.ValoresFinanciamentoPacProposta,
                        mensagemValidacaoValoresFinanciamentoPropostaMaiorPac, true));
                }
            }
        }

        private void ValidarValorFgtsPropostaMenorValorFgtsProposta(ICollection<RetornoPreAtivacaoContrato> retornoValidacao, new_propostaanalisecredito propostaAnaliseCreditoCrm, quote proposta)
        {
            if (propostaAnaliseCreditoCrm != null && proposta != null)
            {
                if (proposta.new_valor_fgts_pac == null)
                {
                    proposta.new_valor_fgts_pac = new CrmDecimal { Value = 0 };
                }
                var valorFgtsProposta = proposta.new_valor_fgts_pac.Value;

                if (propostaAnaliseCreditoCrm.new_valorfgts == null)
                {
                    propostaAnaliseCreditoCrm.new_valorfgts = new CrmDecimal { Value = 0 };
                }
                var valorFgtsPropostaAnaliseCredito = propostaAnaliseCreditoCrm.new_valorfgts.Value;
                var mensagemValidarValoresFgtsPropostaMenorPac =
                    DominioPropostaVenda.ValidarValoresFGTSPropostaPac(propostaAnaliseCreditoCrm, proposta);

                if (!string.IsNullOrEmpty(mensagemValidarValoresFgtsPropostaMenorPac))
                {
                    var mensagemValidacaoValorFgtsMenorPropostaPac =
                        string.Format(Mensagem.MENSAGEM_VALOR_FGTS_CONTIDO_PROPOSTA_MENOR_PAC,
                            Utility.formataMoeda(valorFgtsProposta.ToString()),
                            Utility.formataMoeda(valorFgtsPropostaAnaliseCredito.ToString()));

                    retornoValidacao.Add(new RetornoPreAtivacaoContrato(TipoGrupoValidacao.ValoresDaPropostaSuperiores,
                        TipoErroPreAtivacaoContrato.ValoresFinanciamentoPacProposta,
                        mensagemValidacaoValorFgtsMenorPropostaPac, true));
                }
            }
        }

        private void ValidarValorSubsidioPropostaMenorPac(ICollection<RetornoPreAtivacaoContrato> retornoValidacao, new_propostaanalisecredito propostaAnaliseCreditoCRM, quote proposta)
        {
            if (propostaAnaliseCreditoCRM != null && proposta != null)
            {
                if (proposta.new_valor_subsidio == null)
                {
                    proposta.new_valor_subsidio = new CrmDecimal { Value = 0 };
                }
                var valorSubsidioProposta = proposta.new_valor_subsidio.Value;

                if (propostaAnaliseCreditoCRM.new_valorsubsidio == null)
                {
                    propostaAnaliseCreditoCRM.new_valorsubsidio = new CrmDecimal { Value = 0 };
                }
                var valorSubsidioPropostaAnaliseCredito = propostaAnaliseCreditoCRM.new_valorsubsidio.Value;
                var retornoValidarValoresSubsidioPropostaMenorPac =
                    DominioPropostaVenda.ValidarValorSubsidioPropostaMenorPac(propostaAnaliseCreditoCRM, proposta);

                if (!string.IsNullOrEmpty(retornoValidarValoresSubsidioPropostaMenorPac))
                {
                    var mensagemValidarValoresSubsidioPropostaMenorPac = string.Format(Mensagem.VALOR_SUBSIDIO_PROPOSTA_MENOR_VALOR_SUBSIDIO_PAC, Utility.formataMoeda(valorSubsidioProposta.ToString()), Utility.formataMoeda(valorSubsidioPropostaAnaliseCredito.ToString()));

                    retornoValidacao.Add(new RetornoPreAtivacaoContrato(TipoGrupoValidacao.ValoresDaPropostaSuperiores,
                    TipoErroPreAtivacaoContrato.ValoresFinanciamentoPacProposta, mensagemValidarValoresSubsidioPropostaMenorPac, true));
                }
            }
        }

        private void ValidarValorFinanciamentoPropostaMenorValorFinanciamentoPac(ICollection<RetornoPreAtivacaoContrato> retornoValidacao, new_propostaanalisecredito propostaAnaliseCreditoCrm, quote proposta)
        {
            if (propostaAnaliseCreditoCrm != null && proposta != null)
            {
                if (proposta.new_valor_financiamento_sac == null)
                {
                    proposta.new_valor_financiamento_sac = new CrmDecimal { Value = 0 };
                }
                var valorFinanciamentoSacProposta = proposta.new_valor_financiamento_sac.Value;

                if (proposta.new_valor_financiamento_price == null)
                {
                    proposta.new_valor_financiamento_price = new CrmDecimal { Value = 0 };
                }
                var valorFinanciamentoPriceProposta = proposta.new_valor_financiamento_price.Value;

                if (propostaAnaliseCreditoCrm.new_valorfinanciamento == null)
                {
                    propostaAnaliseCreditoCrm.new_valorfinanciamento = new CrmDecimal { Value = 0 };
                }
                var valorFinanciamentoSacPropostaAnaliseCredito = propostaAnaliseCreditoCrm.new_valorfinanciamento.Value;

                if (propostaAnaliseCreditoCrm.new_valor_financiamento_price == null)
                {
                    propostaAnaliseCreditoCrm.new_valor_financiamento_price = new CrmDecimal { Value = 0 };
                }
                var valorFinanciamentoPricePropostaAnaliseCredito = propostaAnaliseCreditoCrm.new_valor_financiamento_price.Value;
                var retornoValidacaoValorMenorPropostaPac = DominioPropostaVenda.ValidarValoresFinanciamentoPropostaMenorPac(propostaAnaliseCreditoCrm, proposta);

                if (!string.IsNullOrEmpty(retornoValidacaoValorMenorPropostaPac))
                {
                    var mensagemValidacaoValorMenorPropostaPac = string.Format(Mensagem.MENSAGEM_VALORES_FINANCIAMENTO_CONTIDO_PROPOSTA_MENOR_PAC, Utility.formataMoeda(valorFinanciamentoSacProposta.ToString()), Utility.formataMoeda(valorFinanciamentoPriceProposta.ToString()),
                    Utility.formataMoeda(valorFinanciamentoSacPropostaAnaliseCredito.ToString()), Utility.formataMoeda(valorFinanciamentoPricePropostaAnaliseCredito.ToString()));

                    retornoValidacao.Add(new RetornoPreAtivacaoContrato(TipoGrupoValidacao.ValoresDaPropostaSuperiores,
                    TipoErroPreAtivacaoContrato.ValoresFinanciamentoPacProposta, mensagemValidacaoValorMenorPropostaPac, true));
                }
            }
        }

        public string AtivarContrato(Guid oportundadeId)
        {
            var userIdLogado = DominioUsuario.ObterUsuarioLogado();
            var contrato = DominioContrato.ObterPorOportunidadeInformada(oportundadeId, "billingendon", "expireson",
                "customerid", "serviceaddress", "billtoaddress", "statuscode", "new_testemunha1", "new_testemunha2",
                "new_cpf1", "new_cpf2", "new_data_base", "new_tipo_contrato");

            //Pré-ativa o contrato caso o seja rascunho
            if (contrato.statuscode.Value == Contrato.StatusContrato.RASCUNHO)
            {
                DominioOportunidadeVenda.FecharTodasQueEstaoNaFila(oportundadeId);
                if (DominioVendaGarantida.ValidarOportunidadeVendaGarantida(oportundadeId, Entidade.Oportunidade.StatusCode.EM_ANDAMENTO))
                {
                    DominioContrato.EnviarPastaFinanciamento(oportundadeId, false);
                }

                contrato.statuscode = new Status { Value = Contrato.StatusContrato.RASCUNHO_PRE_ATIVO };
                contrato.new_data_pre_ativacao = new CrmDateTime
                {
                    Value = DateTime.Now.ToString(Utility.FORMATO_DATETIME_INGLES)
                };
                contrato.new_pre_ativado_por_id = new Lookup { Value = userIdLogado };
                RepositorioContrato.Atualizar(contrato, true);

                DominioContrato.IncrementarContadorCSG(oportundadeId, contrato);

                return Contrato.StatusContratoRetorno.PRE_ATIVADO;
            }

            contrato.statuscode = new Status { Value = Contrato.StatusContrato.RASCUNHO_ATIVO };
            contrato.new_data_ativacao = new CrmDateTime
            {
                Value = DateTime.Now.ToString(Utility.FORMATO_DATETIME_INGLES)
            };
            contrato.new_ativado_por_id = new Lookup { Value = userIdLogado };
            RepositorioContrato.Atualizar(contrato, true);
            return Contrato.StatusContratoRetorno.ATIVADO;
        }

        private string ValidarRegrasApuracaoRenda(Guid oportundadeId, Guid contratoId)
        {
            try
            {
                var oportunidade = DominioOportunidadeVenda.ObterPorIdTodosOsCampos(oportundadeId);
                if (oportunidade != null && oportunidade.new_venda_garantida != null &&
                    oportunidade.new_venda_garantida.Value && (oportunidade.new_necessita_fiador == null || !oportunidade.new_necessita_fiador.Value))
                {
                    var atibutosPac = new[] { "new_rendatotal", "new_numeropac" };
                    var pac = DominioPropostaAnaliseCredito.ObterPorOportunidade(oportundadeId, atibutosPac);
                    var proposta = DominioPropostaVenda.ObterPorContrato(contratoId, QuoteState.Won);
                    if (proposta != null)
                    {

                        if (pac != null)
                        {
                            var valorPacApurada = pac.new_rendatotal != null ? pac.new_rendatotal.Value : 0;
                            var valorProposta = proposta.new_renda_presumida != null
                                ? proposta.new_renda_presumida.Value
                                : 0;

                            if (valorPacApurada < valorProposta)
                            {
                                var retornoValidacao =
                                    RepositorioPropostaVenda.ValidarNecessidadeDeFiador(
                                    proposta.quoteid.Value.ToString(),
                                    valorPacApurada);
                                if (retornoValidacao.NecessitaFiador)
                                {
                                    return string.Format(Mensagem.MENSAGEM_COMPROMETIMENTO_INVALIDO,
                                        retornoValidacao.PercentualComprometimentoMinimoFiadorUtilizado);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var erro = Utility.ObterDescricaoErro(ex);
                throw new Exception(erro);
            }
            return string.Empty;
        }

        public void AtualizarSistemaAmortizacao(Guid oportundadeId)
        {
            var sistemaAmortizacao = ObterSistemaAmortizacao();
            if (!sistemaAmortizacao.HasValue)
            {
                return;
            }
            var oportunidade = new opportunity
            {
                opportunityid = new Key { Value = oportundadeId },
                new_sistema_amortizacao = new Picklist { Value = sistemaAmortizacao.Value }
            };

            DominioOportunidadeVenda.Atualizar(oportunidade);
        }

        private int? ObterSistemaAmortizacao()
        {
            if (_propostaAnaliseCreditoCrm != null && _propostaCrm != null)
            {
                decimal valorFinanciamentoSac = 0;
                decimal valorFinanciamentoPrice = 0;
                decimal valorFinanciamentoProposta = 0;

                if (_propostaAnaliseCreditoCrm.new_valorfinanciamento != null)
                {
                    valorFinanciamentoSac = _propostaAnaliseCreditoCrm.new_valorfinanciamento.Value;
                }
                if (_propostaAnaliseCreditoCrm.new_valor_financiamento_price != null)
                {
                    valorFinanciamentoPrice = _propostaAnaliseCreditoCrm.new_valor_financiamento_price.Value;
                }
                if (_propostaCrm.new_financiamento != null)
                {
                    valorFinanciamentoProposta = (decimal)_propostaCrm.new_financiamento.Value;
                }

                // Se o valor de financiamento informado na proposta estiver dentro no intervalo dos valores de financiamento SAC e financiamento PRICE,
                // o sistema de amortização será igual ao sistema do maior valor de financiamento (SAC ou PRICE).
                if ((valorFinanciamentoProposta > valorFinanciamentoSac &&
                    valorFinanciamentoProposta <= valorFinanciamentoPrice) ||
                    (valorFinanciamentoProposta > valorFinanciamentoPrice &&
                    valorFinanciamentoProposta <= valorFinanciamentoSac))
                {
                    if (valorFinanciamentoSac > valorFinanciamentoPrice)
                    {
                        return Entidade.Oportunidade.SistemaAmortizacao.SAC;
                    }
                    return Entidade.Oportunidade.SistemaAmortizacao.PRICE;
                }
                // Se o valor de financiamento informado na proposta estiver fora do intervalo dos valores de financiamento SAC e financiamento PRICE,
                // o sistema de amortização será igual ao sistema do menor valor de financiamento (SAC ou PRICE).
                if (valorFinanciamentoSac > valorFinanciamentoPrice)
                {
                    return Entidade.Oportunidade.SistemaAmortizacao.PRICE;
                }
                return Entidade.Oportunidade.SistemaAmortizacao.SAC;
            }

            return null;
        }

        private IEnumerable<RetornoPreAtivacaoContrato> ValidarRegrasIndicacaoPremiadaPreAtivarContrato(Guid oportunidadeId)
        {
            var retorno = new List<RetornoPreAtivacaoContrato>();

            if (_oportunidadeCrm == null)
            {
                _oportunidadeCrm = (opportunity)DominioOportunidadeVenda.ObterPorId(oportunidadeId);
            }

            if (!ValidarRegrasIndicacaoPremiada(_oportunidadeCrm))
            {
                retorno.Add(new RetornoPreAtivacaoContrato(TipoGrupoValidacao.NaoAprovado,
                    TipoErroPreAtivacaoContrato.ValidacaoIndicacaoPremiada, Mensagem.MENSAGEM_INDICACAO_PREMIADA, true));
            }

            return retorno;
        }

        private bool ValidarRegrasIndicacaoPremiada(opportunity oportunidade)
        {

            if (oportunidade == null || oportunidade.new_cheque_indicacao_premiadaid == null)
            {
                return true;
            }
            try
            {
                var listaOportunidadeCheque = DominioOportunidadeVenda.ObterPorChequeIndicado(oportunidade.new_cheque_indicacao_premiadaid.Value);

                foreach (var itemOportunidade in listaOportunidadeCheque)
                {
                    if (itemOportunidade != null && itemOportunidade.statecode != null &&
                        itemOportunidade.statuscode != null)
                    {
                        if (!itemOportunidade.opportunityid.Value.Equals(oportunidade.opportunityid.Value))
                        {
                            if (itemOportunidade.statecode.Value == OpportunityState.Won || itemOportunidade.statuscode.Value == Entidade.Oportunidade.StatusCode.PRE_GANHO)
                            {
                                return false;
                            }
                            if (itemOportunidade.statecode.Value == OpportunityState.Open)
                            {
                                var contrato = ObterContrato(itemOportunidade.opportunityid.Value);
                                if (contrato != null && contrato.statuscode != null)
                                {
                                    var statuContrato = contrato.statuscode.Value;
                                    if (statuContrato == Entidade.Contrato.StatusCode.ATIVO ||
                                        statuContrato == Entidade.Contrato.StatusCode.RASCUNHO_ATIVO ||
                                        statuContrato == Entidade.Contrato.StatusCode.RASCUNHO_PRE_ATIVO)
                                    {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                return true;
            }

            return true;

        }

        public void AjustarPropostaIndicacaoPremiada(Guid oportunidadeId)
        {
            var oportunidade = (opportunity)DominioOportunidadeVenda.ObterPorId(oportunidadeId);

            if (oportunidade != null)
            {
                if (!ValidarRegrasIndicacaoPremiada(oportunidade))
                {
                    var oportunidadeAjuste = new opportunity
                    {
                        opportunityid = new Key { Value = oportunidadeId },
                        new_cheque_indicacao_premiadaid = LookupHelper.Null,
                        description = string.Empty,
                        new_valortotalcnd = CrmMoneyHelper.Null
                    };

                    DominioOportunidadeVenda.Atualizar(oportunidadeAjuste);

                    var proposta = DominioPropostaVenda.ObterMaiorPropostaRevisao(oportunidadeId);
                    if (proposta != null)
                    {
                        var propostaAtualizar = new quote
                        {
                            quoteid = new Key { Value = proposta.quoteid.Value },
                            new_valor_indicacao_premiada = CrmMoneyHelper.Null
                        };

                        DominioPropostaVenda.Atualizar(propostaAtualizar);

                        var isPlanoFlex = true;
                        var planoFinanciamento = DominioPlanoFinanciamento.ObterPorOportunidade(oportunidadeId);
                        if (planoFinanciamento != null && planoFinanciamento.new_classificacao != null)
                        {
                            isPlanoFlex = Entidade.PlanoDeFinanciamento.Classificacao.FLEX ==
                                          planoFinanciamento.new_classificacao.Value;
                        }
                        if (isPlanoFlex)
                        {
                            var produtoProposta =
                                RepositorioProdutoProposta.ObterPorProposta(proposta.quoteid.Value);
                            if (produtoProposta != null && produtoProposta.quotedetailid != null)
                            {
                                var produtoPropostaAtualizar = new quotedetail
                                {
                                    quotedetailid = new Key
                                    {
                                        Value = produtoProposta.quotedetailid.Value,
                                    },
                                    manualdiscountamount = CrmMoneyHelper.Null
                                };

                                RepositorioProdutoProposta.Atualizar(produtoPropostaAtualizar);
                            }
                        }
                    }
                }
            }
        }

        private bool ValidarPlanoFlexBeneficioIss(Guid oportunidadeId, new_planodefinanciamento planoFinanciamento)
        {
            return planoFinanciamento.new_classificacao.Value == Entidade.PlanoDeFinanciamento.Classificacao.FLEX &&
                                    DominioOportunidadeVenda.ValidarOportunidadeBeneficioIss(oportunidadeId);
        }

        #region IDisposable Members
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {

            }
        }
        #endregion
    }
}
