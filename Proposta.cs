using System;
using System.Collections.Generic;
using System.Reflection;
using Mrv.Crm.Repositorio;
using Mrv.Crm.Util;
using Mrv.Crm.ValueObjects;
using Mrv.Crm.WebReference.CrmService.Crm.Service;
using System.Text;
using Mrv.Crm.Util.CrmHelpers;
using Mrv.Crm.ValueObjects.IndicacaoPremiada;
namespace Mrv.Crm.Dominio
{
    public abstract class Proposta : IDisposable
    {
        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {

            }
        }

        #endregion

        private IDictionary<string, object> propriedades = null;
        private BusinessEntity propostaInfo;
        private IRepositorioProposta repositorio;

        private RepositorioProposta _repositorioProposta;
        public RepositorioProposta RepositorioProposta
        {
            get
            {
                if (_repositorioProposta == null)
                {
                    _repositorioProposta = new RepositorioProposta();
                  }
                return _repositorioProposta;
            }
        }

        private RepositorioEmpresaVendedora _repositorioEmpresaVendedora;
        public RepositorioEmpresaVendedora RepositorioEmpresaVendedora
        {
            get
            {
                if (_repositorioEmpresaVendedora == null)
                    _repositorioEmpresaVendedora = new RepositorioEmpresaVendedora();
                return _repositorioEmpresaVendedora;
            }
        }
        private RepositorioLoja _repositorioLoja;
        public RepositorioLoja RepositorioLoja
        {
            get
            {
                if (_repositorioLoja == null)
                    _repositorioLoja = new RepositorioLoja();
                return _repositorioLoja;
            }
        }
        private SystemUser _dominioUsuario;
        public SystemUser DominioUsuario
        {
            get
            {
                if (_dominioUsuario == null)
                    _dominioUsuario = new SystemUser();
                return _dominioUsuario;
            }
        }

        private Utility _util;
        private Utility Util
        {
            get
            {
                if (_util == null)
                    _util = new Utility();
                return _util;
            }
        }
        private Dominio.Oportunidade _dominioOportunidade;
        public Dominio.Oportunidade DominioOportunidade
        {
            get
            {
                if (_dominioOportunidade == null)
                    _dominioOportunidade = new Mrv.Crm.Dominio.Oportunidade();
                return _dominioOportunidade;
            }
        }
        private Dominio.Empreendimento _dominioEmpreendimento;
        public Dominio.Empreendimento DominioEmpreendimento
        {
            get
            {
                if (_dominioEmpreendimento == null)
                    _dominioEmpreendimento = new Mrv.Crm.Dominio.Empreendimento();
                return _dominioEmpreendimento;
            }
        }
        private Dominio.Cidade _dominioCidade;
        public Dominio.Cidade DominioCidade
        {
            get
            {
                if (_dominioCidade == null)
                    _dominioCidade = new Mrv.Crm.Dominio.Cidade();
                return _dominioCidade;
            }
        }
        private Oportunidade _dominioOportunidadeVenda;
        public Oportunidade DominioOportunidadeVenda
        {
            get
            {
                if (_dominioOportunidadeVenda == null)
                    _dominioOportunidadeVenda = new Oportunidade();
                return _dominioOportunidadeVenda;
            }
        }

        private ClientePotencial _dominioClientePotencial;
        public ClientePotencial DominioClientePotencial
        {
            get
            {
                if (_dominioClientePotencial == null)
                    _dominioClientePotencial = new ClientePotencial();
                return _dominioClientePotencial;
            }
        }

        private RepositorioPropostaAnaliseCredito _repositorioPropostaAnaliseCredito;
        public RepositorioPropostaAnaliseCredito RepositorioPropostaAnaliseCredito
        {
            get
            {
                if (_repositorioPropostaAnaliseCredito == null)
                    _repositorioPropostaAnaliseCredito = new RepositorioPropostaAnaliseCredito();
                return _repositorioPropostaAnaliseCredito;
            }
        }

        private PropostaAnaliseCredito _dominioPropostaAnaliseCredito;
        public PropostaAnaliseCredito DominioPropostaAnaliseCredito
        {
            get
            {
                if (_dominioPropostaAnaliseCredito == null)
                    _dominioPropostaAnaliseCredito = new PropostaAnaliseCredito();
                return _dominioPropostaAnaliseCredito;
            }
        }

        private ModuloVillageFinacBanc _dominioModuloVillageFinacBanc;
        public ModuloVillageFinacBanc DominioModuloVillageFinacBanc
        {
            get
            {
                if (_dominioModuloVillageFinacBanc == null)
                    _dominioModuloVillageFinacBanc = new ModuloVillageFinacBanc();
                return _dominioModuloVillageFinacBanc;
            }
        }

        private Modulo _dominioModulo;
        public Modulo DominioModulo
        {
            get
            {
                if (_dominioModulo == null)
                    _dominioModulo = new Modulo();
                return _dominioModulo;
            }
        }

        private Bloco _dominioBloco;
        public Bloco DominioBloco
        {
            get
            {
                if (_dominioBloco == null)
                    _dominioBloco = new Bloco();
                return _dominioBloco;
            }
        }

        private RepositorioMotorRegrasProposta _repositorioMotorRegrasProposta;
        public RepositorioMotorRegrasProposta RepositorioMotorRegrasProposta
        {
            get
            {
                if (_repositorioMotorRegrasProposta == null)
                    _repositorioMotorRegrasProposta = new RepositorioMotorRegrasProposta();
                return _repositorioMotorRegrasProposta;
            }
        }

        private Sinergia _dominioSinergia;
        protected Sinergia DominioSinergia
        {
            get
            {
                return _dominioSinergia ?? (_dominioSinergia = new Sinergia());
            }
        }

        internal Proposta(Type tipo)
        {
            this.repositorio = RepositorioFactory.InstanciarRepositorioProposta(tipo);
        }

        internal Proposta(BusinessEntity propostaInfo)
            : this(propostaInfo.GetType())
        {

            if (null == propostaInfo)
                throw new ArgumentNullException("Proposta", "Não é possível validar a regra para geração de cheques e notas promissórias, pois a entidade quote ou new_proposta_renegociação é inválida.");

            InicializaAtributosDeNegocioInternos(propostaInfo);
        }

        private ParametroComercial _dominioParametroComercial;
        public ParametroComercial DominioParametroComercial
        {
            get
            {
                if (_dominioParametroComercial == null)
                    _dominioParametroComercial = new ParametroComercial();
                return _dominioParametroComercial;
            }
        }

        private RelacaoCliente _dominioRelacaoCliente;
        public RelacaoCliente DominioRelacaoCliente
        {
            get
            {
                if (_dominioRelacaoCliente == null)
                    _dominioRelacaoCliente = new RelacaoCliente();
                return _dominioRelacaoCliente;
            }
        }

        private PlanoFinanciamento _dominioPlanoFinanciamento;
        public PlanoFinanciamento DominioPlanoFinanciamento
        {
            get
            {
                if (_dominioPlanoFinanciamento == null)
                    _dominioPlanoFinanciamento = new PlanoFinanciamento();
                return _dominioPlanoFinanciamento;
            }
        }

        private IndicacaoPremiada _dominioIndicacaoPremiada;
        public IndicacaoPremiada DominioIndicacaoPremiada
        {
            get
            {
                if (_dominioIndicacaoPremiada == null)
                    _dominioIndicacaoPremiada = new IndicacaoPremiada();
                return _dominioIndicacaoPremiada;
            }
        }

        private PropostaVenda _dominioPropostaVenda;
        public PropostaVenda DominioPropostaVenda
        {
            get
            {
                if (_dominioPropostaVenda == null)
                    _dominioPropostaVenda = new PropostaVenda();
                return _dominioPropostaVenda;
            }
        }

        private RepositorioHistoricoAtividade _repositorioHistoricoAtividade;
        public RepositorioHistoricoAtividade RepositorioHistoricoAtividade
        {
            get
            {
                if (_repositorioHistoricoAtividade == null)
                    _repositorioHistoricoAtividade = new RepositorioHistoricoAtividade();
                return _repositorioHistoricoAtividade;
            }
        }

        private PagamentoIndicacao _dominioPagamento;
        private PagamentoIndicacao DominioPagamento
        {
            get
            {
                return _dominioPagamento ?? (_dominioPagamento = new PagamentoIndicacao());
            }
        }

        /// <summary>
        /// Estrutura de mensagens.
        /// </summary>
        public struct Mensagem
        {
            public const string STATUS_PROPOSTA_ANALISE_CREDITO_VALORES_INVALIDOS = "Os valores aprovados pelo crédito são menores que os solicitados na proposta original";
            public const string PROPOSTA_ANALISE_CREDITO_PLANO_FINACIAMENTO_INEXISTENTE = "Não existe plano de financiamento associado a oportunidade.";
            public const string PROPOSTA_ANALISE_CREDITO_INEXISTENTE = "Favor solicitar uma Análise de Crédito para a Oportunidade.";
            public const string DATA_INCLUSAO_LOJA_NULA = "Data de inclusão da Loja no módulo de crédito não pode ser nula. Favor entrar em contato com o Administrador do Sistema.";
            public const string MENSAGEM_FORMA_PAGAMENTO_JUSTIFICATIVA_NULO = "Os campos Forma de Pagamento ou Justificativa de Opção de Serviços de Registro de Contrato devem ser preenchidos";
            public const string MENSAGEM_FORMA_PAGAMENTO_JUSTIFICATIVA_NAO_NULO = "Os campo Forma de Pagamento e Justificativa de Opção de Serviços de Registro de Contrato estão preenchidos.\n Somente um dos dois campos deve ser preenchido.";
            public const string MENSAGEM_VALOR_TAXA_TIPO_PAGAMENTO_PARCELAMENTO_TAXA_NULO = "Os campos Valor da taxa, Tipo de Pagamento e Parcelamento Taxas devem ser preenchidos.";
            public const string MENSAGEM_VALOR_TAXA_TIPO_PAGAMENTO_PARCELAMENTO_TAXA_NAO_NULO = "Os campos Valor da taxa, Tipo de Pagamento e Parcelamento Taxas não devem ser preenchidos.";
            public const string CIDADE_NAO_DEFINIDA = "Cidade não definida no empreendimento.";
            public const string VALOR_AVALIACAO_DIFERENTE = "O valor de avaliação atual (R${0:N2}) é diferente do valor de avaliação que estava vinculada na unidade no momento da aprovação do Crédito (R${1:N2}).";
            public const string VALOR_TOTAL_PROPOSTA_DIFERENTE = "O valor total da proposta atual (R${0:N2}) é diferente do valor total da proposta que estava vinculada na PAC no momento da aprovação do Crédito (R${1:N2}).";
            public const string PAC_APROVADA_REUTILIZADA = "Ganho da proposta validada com sucesso.";
            public const string PAC_EM_DISCUSSAO_REUTILIZADA = "Status da PAC Reutilizada alterado para Em Discussão no momento do ganho da proposta devido a divergência de valores.";
            public const string PAC_RENDA_APURADA_ALTERADA = "O valor da  {0} {1} está diferente que o atual valor aprovado na PAC {2}. Para ganhar a proposta será necessário contestar a PAC ou alterar os valores da proposta (gerar contraproposta).";
            public const string PAC_RENDA_APURADA_ALTERADA_PRE_ATIVAR = "O valor da {0} {1} está diferente que o atual valor aprovado na PAC {2}. Para pré-ativar o contrato será necessário contestar a PAC ou ajustar a {3} por meio do processo de cancelamento do contrato e revisão da proposta.";
            public const string VALOR_COMPRA_VENDA_DIVERGENTE_VALOR_PROPOSTA = "O valor de compra venda aprovado na PAC é diferente do valor total da proposta. Para ganhar a proposta será necessário contestar a PAC ou ajustar o valor total da proposta (R$ {0:N2}) no valor da PAC (R$ {1:N2}) - gerar contraproposta.";
            public const string VALOR_COMPRA_VENDA_DIVERGENTE_VALOR_PROPOSTA_CONTRATO = "O valor de compra venda aprovado na PAC é diferente do valor total da proposta. Para pré-ativar o contrato será necessário contestar a PAC ou ajustar o valor total da proposta (R$ {0:N2}) no valor da PAC (R$ {1:N2}) por meio do processo de cancelamento do contrato e revisão da proposta.";
            public const string VALORES_FGTS_PROPOSTA_MENOR_VALORES_FINANCIAMENTO_PAC = "O valor de FGTS da PAC contido na proposta ({0}) esta menor que o atual valor aprovado na PAC ({1}). Para ganhar a proposta será necessário contestar a PAC ou alterar os valores da proposta (gerar contraproposta).";
            public const string VALORES_STATUS_PROPOSTA_DIFERENTE_PAC = "O status atual da PAC ({0}) é diferente do status da PAC contido na proposta ({1}). Para ganhar a proposta será necessário aguardar aprovação da PAC ou reapurar as regras da proposta (gerar contraproposta).";
            public const string VALORES_FINANCIAMENTO_PROPOSTA_MAIOR_VALORES_FINANCIAMENTO_PAC = "O valor de financiamento informado na proposta ({0}) é maior que o valor aprovado na PAC ({1} + {2}). Para ganhar a proposta será necessário contestar a PAC ou alterar os valores da proposta (gerar contraproposta).";
            public const string VALORES_FGTS_PROPOSTA_MAIOR_VALORES_FINANCIAMENTO_PAC = "O valor de FGTS informado na proposta ({0}) é maior que o valor aprovado na PAC ({1}). Para ganhar a proposta será necessário contestar a PAC ou alterar os valores da proposta (gerar contraproposta).";
            public const string VALORES_FINANCIAMENTO_PROPOSTA_MENOR_VALORES_FINANCIAMENTO_PAC = "Os valores de financiamento da PAC contidos na proposta (SAC {0} e PRICE {1}) estão menores que os atuais valores aprovados na PAC (SAC {2} e PRICE {3}). Para ganhar a proposta será necessário contestar a PAC ou alterar os valores da proposta (gerar contraproposta).";
            public const string VALOR_SUBSIDIO_PROPOSTA_MENOR_VALOR_SUBSIDIO_PAC = "O valor de subsídio da PAC contido na proposta ({0}) esta menor que o atual valor aprovado na PAC ({1}). Para ganhar a proposta será necessário contestar a PAC ou alterar os valores da proposta (gerar contraproposta).";
            public const string ERRO_OBTER_PARAMETROS_COMERCIAL = "Erro ao Obter Parametros Comerciais para Calculo do Delta";
            public const string TIPO_CALCULO_INVALIDO = "Tipo do calculo do Delta Inválido";
            public const string TIPO_MARGEM_INVALIDO = "Tipo de margem do Delta Inválido";
            public const string RENDA_APURADA_CREDITO_PROPOSTA = "renda apurada contida na PAC no momento do ganho da proposta";
            public const string RENDA_PRESUMIDA = "renda presumida contida na proposta";
        }

        public const string EM_DISCUSSAO = "Em Discussão";
        private List<int> CodigosStatusPacAprovadas = new List<int>() { 14, 4, 8, 7, 320 };

        #region Atributos
        // TODO: OS ATRIBUTOS DESTA CLASSE SÃO OS MESMO UTILIZADOS NA PROPOSTA DE VENDA E NA PROPOSTA DE RENEGOCIAÇÃO.
        // SENDO ASSIM, NA IMPLEMENTAÇÃO DA CLASSE PROPOSTARENEGOCIACÃO ELA HERADARÁ ESTES VALORES NATURALMENTE.

        /// <summary>
        /// Repositorio de dados da proposta.
        /// </summary>
        protected IRepositorioProposta Repositorio
        {
            get { return repositorio; }
        }

        /// <summary>
        /// Atributo que permite o acesso a aos valores de uma lista.
        /// </summary>
        /// <param name="nomeAtributo">Nome do atributo que se deseja recuperar.</param>
        /// <returns></returns>
        internal object this[string nomeAtributo]
        {
            get
            {
                if (propriedades == null)
                    return null;

                if (!propriedades.ContainsKey(nomeAtributo))
                    return null;

                return (object)propriedades[nomeAtributo];
            }
        }

        /// <summary>
        /// Indica se a validação das condições de pagamento da proposta é uma validação que ocorre somente no contexto interno da geração do contrato ou,
        /// pode ser exposto para a utilização em outros processos ou regras.
        /// </summary>
        protected bool EValidacaoPrivada
        {
            get
            {
                return propostaInfo != null;
            }
        }

        /// <summary>
        /// Informa se foi negociada uma entrada como sinal.
        /// </summary>
        internal bool PossuiSinalEntrada
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_valorsinalavista"].ToString() != "" &&
                    this["new_datadopagamento"].ToString() != "";
            }
        }

        /// <summary>
        /// Informando se a proposta possui um parcelamento do sinal.
        /// </summary>
        internal bool PossuiDivisaoDeSinal
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_tipodepagamentodivsinal"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe divisão de Sinal em uma parcela.
        /// </summary>
        internal bool PossuiDivisaoDeSinal1
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelas"].ToString() != "" &&
                    this["new_valordaparcela"].ToString() != "" &&
                    this["new_datadevencimento1parcela"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe divisão de Sinal em duas parcela.
        /// </summary>
        internal bool PossuiDivisaoDeSinal2
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelassinal1"].ToString() != "" &&
                    this["new_valordaparcelasinal1"].ToString() != "" &&
                    this["new_datadevencimento1parcelasinal1"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe divisão de Sinal em três parcela.
        /// </summary>
        internal bool PossuiDivisaoDeSinal3
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelassinal2"].ToString() != "" &&
                    this["new_valordasparcelassinal2"].ToString() != "" &&
                    this["new_datadevencimento1parcelasinal2"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe divisão de Sinal em quatro parcela.
        /// </summary>
        internal bool PossuiDivisaoDeSinal4
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelassinal3"].ToString() != "" &&
                    this["new_valordasparcelassinal3"].ToString() != "" &&
                    this["new_datadevencimento1parcelasinal3"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe divisão uma possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal1
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais"].ToString() != "" &&
                    this["new_valordaparcelamensal"].ToString() != "" &&
                    this["new_datadepagamentomensal"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a segunda possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal2
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais1"].ToString() != "" &&
                    this["new_valordaparcelamensal1"].ToString() != "" &&
                    this["new_datado1pagamentomensal1"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a terceira possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal3
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais2"].ToString() != "" &&
                    this["new_valordaparcelamensal2"].ToString() != "" &&
                    this["new_datado1pagamentomensal2"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a quarta possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal4
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais3"].ToString() != "" &&
                    this["new_valordaparcelamensal3"].ToString() != "" &&
                    this["new_datado1pagamentomensal3"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a quinta possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal5
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais4"].ToString() != "" &&
                    this["new_valordaparcelamensal4"].ToString() != "" &&
                    this["new_datado1pagamentomensal4"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a sexta possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal6
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais5"].ToString() != "" &&
                    this["new_valordaparcelamensal5"].ToString() != "" &&
                    this["new_datado1pagamentomensal5"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a sexta possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal7
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais6"].ToString() != "" &&
                    this["new_valordaparcelamensal6"].ToString() != "" &&
                    this["new_datado1pagamentomensal6"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a sexta possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal8
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais7"].ToString() != "" &&
                    this["new_valordaparcelamensal7"].ToString() != "" &&
                    this["new_datado1pagamentomensal7"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a sexta possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal9
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais8"].ToString() != "" &&
                    this["new_valordaparcelamensal8"].ToString() != "" &&
                    this["new_datado1pagamentomensal8"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a sexta possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal10
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais9"].ToString() != "" &&
                    this["new_valordaparcelamensal9"].ToString() != "" &&
                    this["new_datado1pagamentomensal9"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a sexta possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal11
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais10"].ToString() != "" &&
                    this["new_valordaparcelamensal10"].ToString() != "" &&
                    this["new_datado1pagamentomensal10"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a sexta possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal12
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais11"].ToString() != "" &&
                    this["new_valordaparcelamensal11"].ToString() != "" &&
                    this["new_datado1pagamentomensal11"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a sexta possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal13
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais12"].ToString() != "" &&
                    this["new_valordaparcelamensal12"].ToString() != "" &&
                    this["new_datado1pagamentomensal12"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a sexta possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal14
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais13"].ToString() != "" &&
                    this["new_valordaparcelamensal13"].ToString() != "" &&
                    this["new_datado1pagamentomensal13"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a sexta possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal15
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais14"].ToString() != "" &&
                    this["new_valordaparcelamensal14"].ToString() != "" &&
                    this["new_datado1pagamentomensal14"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a sexta possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal16
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais15"].ToString() != "" &&
                    this["new_valordaparcelamensal15"].ToString() != "" &&
                    this["new_datado1pagamentomensal15"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a sexta possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal17
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais16"].ToString() != "" &&
                    this["new_valordaparcelamensal16"].ToString() != "" &&
                    this["new_datado1pagamentomensal16"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a sexta possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal18
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais17"].ToString() != "" &&
                    this["new_valordaparcelamensal17"].ToString() != "" &&
                    this["new_datado1pagamentomensal17"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a sexta possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal19
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais18"].ToString() != "" &&
                    this["new_valordaparcelamensal18"].ToString() != "" &&
                    this["new_datado1pagamentomensal18"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a sexta possibilidade de parcelamento mensal.
        /// </summary>
        internal bool PossuiDivisaoDeMensal20
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasmensais19"].ToString() != "" &&
                    this["new_valordaparcelamensal19"].ToString() != "" &&
                    this["new_datado1pagamentomensal19"].ToString() != "";
            }
        }


        /// <summary>
        /// Informa se a proposta possui um financiamento.
        /// </summary>
        internal bool PossuiFinancimamento
        {
            get
            {
                return
                       EValidacaoPrivada &&
                       this["new_financiamento"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se a proposta utiliza o FGTS
        /// </summary>
        internal bool UtilizaFGTS
        {
            get
            {
                return
                       EValidacaoPrivada &&
                       this["new_valordofgts"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se o cliente dispoe de recursos próprios.
        /// </summary>
        internal bool DispoeDeRecursosProprios
        {
            get
            {
                return
                       EValidacaoPrivada &&
                       this["new_recurso_proprio"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se a proposta possui valores de assessoria.
        /// </summary>
        internal bool PossuiValoresDeAssessoria
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_tipodepagamentoassessoria"].ToString() != "" &&
                    this["new_tipodepagamentoassessoria"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se a possui divisão de assessoria em uma parcela.
        /// </summary>
        internal bool PossuiDivisaoDeAssessoria1
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasassessoria"].ToString() != "" &&
                    this["new_valordasparcelasassessoria"].ToString() != "" &&
                    this["new_datadevencimento1parcelaassessoria"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se a possui divisão de assessoria em duas parcela.
        /// </summary>
        internal bool PossuiDivisaoDeAssessoria2
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasassessoria1"].ToString() != "" &&
                    this["new_valordasparcelasassessoria"].ToString() != "" &&
                    this["new_datadevencimento1parcelaassessoria"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe uma possibilidade de transferencia.
        /// </summary>
        internal bool PossuiDivisaoTransferencia1
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtde_parcelas_transf1"].ToString() != "" &&
                    this["new_valor_parcela_transf1"].ToString() != "" &&
                    this["new_vencimento_1parcela_transf1"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a segunda possibilidade de transferencia.
        /// </summary>
        internal bool PossuiDivisaoTransferencia2
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtde_parcelas_transf2"].ToString() != "" &&
                    this["new_valor_parcela_transf2"].ToString() != "" &&
                    this["new_vencimento1parcela_transf2"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a terceira possibilidade de transferencia.
        /// </summary>
        internal bool PossuiDivisaoTransferencia3
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtde_parcelas_transf3"].ToString() != "" &&
                    this["new_valor_parcela_transf3"].ToString() != "" &&
                    this["new_vencimento1parcela_transf3"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe uma possibilidade de parcelamento para Intermediaria.
        /// </summary>
        internal bool PossuiDivisaoIntermediaria1
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdeintermediarias"].ToString() != "" &&
                    this["new_valosdaparcelaintermediaria"].ToString() != "" &&
                    this["new_datadepagamentointermediaria"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a segunda possibilidade de parcelamento para Intermediaria.
        /// </summary>
        internal bool PossuiDivisaoIntermediaria2
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasintermediarias1"].ToString() != "" &&
                    this["new_valordaparcelaintermediaria1"].ToString() != "" &&
                    this["new_datadepagamentointermediria1"].ToString() != "";
            }
        }

        /// <summary>
        /// Verifica se existe a terceira possibilidade de parcelamento para Intermediaria.
        /// </summary>
        internal bool PossuiDivisaoIntermediaria3
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtdedeparcelasintermediarias2"].ToString() != "" &&
                    this["new_valordaparcelaintermediaria2"].ToString() != "" &&
                    this["new_datadepagamentointermediaria2"].ToString() != "";
            }
        }

        internal bool PossuiDiferencaFinanciamento1
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtd_parcela_df1"].ToString() != "" &&
                    this["new_valor_parcela_df1"].ToString() != "" &&
                    this["new_dat_vencimento_parceladf1"].ToString() != "";
            }
        }
        internal bool PossuiDiferencaFinanciamento2
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtd_parcela_df2"].ToString() != "" &&
                    this["new_valor_parcela_df2"].ToString() != "" &&
                    this["new_dat_vencimento_parceladf2"].ToString() != "";
            }
        }

        internal bool PossuiDiferencaFinanciamento3
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtd_parcela_df3"].ToString() != "" &&
                    this["new_valor_parcela_df3"].ToString() != "" &&
                    this["new_dat_vencimento_parceladf3"].ToString() != "";
            }
        }

        internal bool PossuiDiferencaFinanciamento4
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtd_parcela_df4"].ToString() != "" &&
                    this["new_valor_parcela_df4"].ToString() != "" &&
                    this["new_dat_vencimento_parceladf4"].ToString() != "";
            }
        }

        internal bool PossuiDiferencaFinanciamento5
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtd_parcela_df5"].ToString() != "" &&
                    this["new_valor_parcela_df5"].ToString() != "" &&
                    this["new_dat_vencimento_parceladf5"].ToString() != "";
            }
        }

        internal bool PossuiDiferencaFinanciamento6
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtd_parcela_df6"].ToString() != "" &&
                    this["new_valor_parcela_df6"].ToString() != "" &&
                    this["new_dat_vencimento_parceladf6"].ToString() != "";
            }
        }

        internal bool PossuiDiferencaFinanciamento7
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtd_parcela_df7"].ToString() != "" &&
                    this["new_valor_parcela_df7"].ToString() != "" &&
                    this["new_dat_vencimento_parceladf7"].ToString() != "";
            }
        }

        internal bool PossuiDiferencaFinanciamento8
        {
            get
            {
                return
                    EValidacaoPrivada &&
                    this["new_qtd_parcela_df8"].ToString() != "" &&
                    this["new_valor_parcela_df8"].ToString() != "" &&
                    this["new_dat_vencimento_parceladf8"].ToString() != "";
            }
        }

        #endregion

        /// <summary>
        /// Recupera a periodicidade de pagamento das parcelas.
        /// </summary>
        public Parcela.PeriodicidadePagamento ObtemPeriodicidadePagamento()
        {
            return EValidacaoPrivada
                ? Parcela.PeriodicidadePagamento.Mensal
                : PossuiDivisaoDeSinal1
                    ? (Parcela.PeriodicidadePagamento)int.Parse(propriedades["new_periodicidadesinal"].ToString())
                    : PossuiDivisaoDeSinal2
                        ? (Parcela.PeriodicidadePagamento)int.Parse(propriedades["new_periodicidadesinal1"].ToString())
                        : PossuiDivisaoDeSinal3
                            ? (Parcela.PeriodicidadePagamento)int.Parse(propriedades["new_periodicidadesinal2"].ToString())
                            : PossuiDivisaoDeSinal4
                                ? (Parcela.PeriodicidadePagamento)int.Parse(propriedades["new_periodicidadesinal3"].ToString())
                                : PossuiDivisaoDeAssessoria1
                                    ? (Parcela.PeriodicidadePagamento)int.Parse(propriedades["new_periodicidadeassessoria"].ToString())
                                    : PossuiDivisaoDeAssessoria2
                                        ? (Parcela.PeriodicidadePagamento)int.Parse(propriedades["new_periodicidadeassessoria1"].ToString())
                                        : Parcela.PeriodicidadePagamento.Mensal;
        }

        /// <summary>
        /// Inicializa uma lista de atributos internos da classe.
        /// </summary>
        /// <remarks>
        /// Estes atributos tem a finalidade de equalizar o acesso aos valores de quote e new_proposta_renegociacao.
        /// Que por conta do levantamento de requisitos constatou-se que deveria possuir os mesmos atributos. E como o MS CRM
        /// não possui o conceito de herança foi necessário criar duas entidades identicas.
        /// </remarks>
        /// <!--
        /// Autor: rsimoes
        /// Data: 2/4/2009 10:00:06 AM
        /// -->
        protected void InicializaAtributosDeNegocioInternos(BusinessEntity proposta)
        {
            this.propostaInfo = proposta;
            propriedades = Utility.Explorador.Propriedade.ConverterParaListaChaveValor(proposta, proposta.GetType().GetProperties());
        }

        /// <summary>
        /// Recupera uma proposta a partir do seu identificador.
        /// </summary>
        /// <typeparam name="T">Proposta de venda ou de renegociação.</typeparam>
        /// <param name="propostaId">Identificador da prosposta.</param>
        /// <returns>Instância da classe que representa uma proposta no sistema.</returns>
        public abstract BusinessEntity ObterPorId(Guid propostaId);

        /// <summary>
        /// Recupera uma proposta a partir de uma renegociação.
        /// </summary>
        /// <param name="renegociacaoId">Identificador da renegociação.</param>
        /// <returns>Instância da classe new_proposta_renegociacao</returns>
        public abstract BusinessEntity ObterPorOportunidade(Guid oportunidadeId);

        public BusinessEntityCollection ObterPorOportunidade(Guid oportunidadeId, string[] campos)
        {
            return RepositorioProposta.ObterPorOportunidade(oportunidadeId, campos);
        }

        /// <summary>
        /// Fecha uma proposta.
        /// </summary>
        /// <param name="oportunidadeId">Identificador da proposta que está sendo fechada.</param>
        /// <param name="razaoStatus">Status do fechamento da proposta</param>
        internal void Fecha(BusinessEntity propostaInfo, int razaoStatus)
        {
            // Verificando se as propriedades internas da classe já foram carregadas alguma vez.
            // Isto porque existe um construtor interno que espera uma instância da classe quote ou new_proposta_renegociacao
            // Para preencher estes atributos.
            if (this[propostaInfo.GetType().Name + "id"] == null || !string.IsNullOrEmpty(this[propostaInfo.GetType().Name + "id"].ToString()))
                this.InicializaAtributosDeNegocioInternos(propostaInfo);

            // Obtendo a intancia da chave primária da entidaed proposta.
            PropertyInfo propriedade = propostaInfo.GetType().GetProperty(propostaInfo.GetType().Name + "id");

            // Utilizando o Explorador para recuprar o valor da chave primária da entidade proposta.
            Guid propostaId = (Guid)Utility.Explorador.Propriedade.ObterValor(propostaInfo, propriedade);

            Fecha(propostaId, razaoStatus);
        }

        /// <summary>
        /// Fecha uma proposta.
        /// </summary>
        /// <param name="oportunidadeId">Identificador da proposta que está sendo fechada.</param>
        /// <param name="razaoStatus">Status do fechamento da proposta</param>
        /// <returns>Instância de uma entidade quote ou new_proposta_renegociacao</returns>
        protected abstract void Fecha(Guid propostaId, int razaoStatus);

        /// <summary>
        /// Cria uma instância especializada de uma proposta de venda ou de renegociação.
        /// </summary>
        /// <param name="tipoOportunidade">proposta de renegociação ou de venda.</param>
        /// <returns>Instância da classe Wrapper para validação das regras de negócio das oportnidades.</returns>
        internal static Proposta Criar(BusinessEntity tipo)
        {
            return PropostaFactory.Criar(tipo);
        }

        /// <summary>
        /// Cria uma instância especializada de uma proposta de venda ou de renegociação.
        /// </summary>
        /// <param name="tipoOportunidade">proposta de renegociação ou de venda.</param>
        /// <returns>Instância da classe Wrapper para validação das regras de negócio das oportnidades.</returns>
        public static Proposta Criar(Type tipo)
        {
            return PropostaFactory.Criar(tipo);
        }

        public quote ObterGanhaPorOportunidadeVenda(Guid oportunidadeId, params string[] atributos)
        {
            return RepositorioProposta.ObterGanhaPorOportunidadeVenda(oportunidadeId, atributos);
        }

        public abstract new_proposta_renegociacao ObterGanhaPorOportunidadeRenegociacao(Guid oportunidadeRenegociacaoId);

        public void ValidarVendaTaxa(Guid propostaId)
        {
            new_empresa_vendedora empresa = (new_empresa_vendedora)RepositorioEmpresaVendedora.ObterPorProposta(propostaId);
            if (empresa == null)
                throw new ApplicationException("Não foi possível recupera a empresa associada ao empreendimento.");

            /// Empresa que inicia com E(E0005) não pode vender taxa de despachante
            if (string.IsNullOrEmpty(empresa.new_cdigoempresasap6) || empresa.new_cdigoempresasap6[0].Equals('E'))
                throw new ApplicationException(Entidade.Aditivo.OPCAO_REGISTRO_CONTRATO.PROPOSTA_OPCAO_REGISTRO_CONTRATO);
        }

        public virtual void ValidaParaGanhar(Guid propostaId, string[] equipes) { }


        public virtual void FecharEmRascunho(Guid propostaId, string equipe) { }

        public abstract string FecharVariasPropostasEmRascunhoEOportunidade(string propostaId, string equipe);

        public abstract PropostaVenda.Parcelas[] ObterParcelasParaBoleto(quote proposta, RequestVendaServico.TipoServico tipo, string tipoParcela, product produto);

        public quote ObterMaiorPropostaRevisao(Guid oportunidadeId, params string[] atributos)
        {
            return RepositorioProposta.ObterMaiorPropostaRevisao(oportunidadeId, atributos);
        }

        public virtual void FecharProposta(Guid propostaId, PropostaVenda.StatusCode status, string descricao) { }
        public virtual void FecharProposta(Guid propostaId, PropostaVenda.StatusCode status, string descricao, string assunto) { }

        public virtual void CancelarPropostaCredito(string idOportunidade) { }
        public virtual void CancelarPropostaCredito(string idOportunidade, string assuntoFechamento, string mensagemFechamento, bool perderAndamento, bool executaFluxoComercial) { }
        public virtual void CancelarPropostaQueda(string idOportunidade, int diasReservaQueda) { }
        public virtual void CancelarProposta(string idOportunidade, bool quedaNaOportunidade, bool mrvCredito, int diasReservaQueda) { }
        public virtual void CancelarProposta(string idOportunidade, bool quedaNaOportunidade, bool mrvCredito, int diasReservaQueda, string assunto, string descricao, bool perderAndamento, bool executaFluxoComercial) { }

        public abstract string ValidaDataBase(DateTime dataBase);

        public string ValidaProposta(Guid idProposta)
        {
            string strMensagem = string.Empty;
            DateTime dtHoje = Utility.ParseDateTime(string.Format("05/{0}/{1}", DateTime.Today.ToString("MM"), DateTime.Today.ToString("yyyy")));
            //Culture
            System.Globalization.CultureInfo oCulture = new System.Globalization.CultureInfo("pt-BR");
            System.Globalization.DateTimeFormatInfo dtFormatInfo = oCulture.DateTimeFormat;

            //Validação se existe alguma data na proposta inferior a data base;
            quote oQuote = this.RepositorioProposta.Obter(idProposta, true);
            if (oQuote.new_data_base_contrato == null)
                return "Informe a data base do contrato";
            DateTime DataBase = Mrv.Crm.Util.Utility.ParseDateTime(oQuote.new_data_base_contrato.date);
            if (oQuote != null)
            {
                //Verificar se o tipo de pagamento é diferente de espécie se for validar a data, se data inferior a data base, não deixar colocar a data.
                //Assessoria
                if (oQuote.new_tipodepagamentoassessoria != null && oQuote.new_tipodepagamentoassessoria.Value != 3)//Diferente de espécie.
                {
                    //Assessoria 1
                    if (oQuote.new_datadevencimento1parcelaassessoria != null && Utility.ParseDateTime(oQuote.new_datadevencimento1parcelaassessoria.date) < DataBase)
                        return "Assessoria 1: Vencimento da parcela de assessoria é inferior a data base.";
                    //Assessoria 2
                    if (oQuote.new_datadevencimento1parcelaassessoria1 != null && Utility.ParseDateTime(oQuote.new_datadevencimento1parcelaassessoria1.date) < DataBase)
                        return "Assessoria 2: Vencimento da parcela de assessoria é inferior a data base.";
                }

                //Sinal a vista
                if (oQuote.new_tipo_pagamento_sinal != null && oQuote.new_tipo_pagamento_sinal.Value != 3)//Diferente de espécie.
                {
                    if (oQuote.new_datadopagamento != null && Utility.ParseDateTime(oQuote.new_datadopagamento.date) < DataBase)
                        return "Sinal á vista: Vencimento da parcela de sinal é inferior a data base.";
                }

                //Sinal divido
                if (oQuote.new_divisaosinal != null && oQuote.new_divisaosinal.Value == true)
                {
                    //Sinal 1
                    if (oQuote.new_datadevencimento1parcela != null && Utility.ParseDateTime(oQuote.new_datadevencimento1parcela.date) < DataBase)
                        return "Sinal 1: Vencimento da parcela sinal é inferior a data base.";

                    //Sinal 2
                    if (oQuote.new_datadevencimento1parcelasinal1 != null && Utility.ParseDateTime(oQuote.new_datadevencimento1parcelasinal1.date) < DataBase)
                        return "Sinal 2: Vencimento da parcela sinal é inferior a data base.";

                    //Sinal 3
                    if (oQuote.new_datadevencimento1parcelasinal2 != null && Utility.ParseDateTime(oQuote.new_datadevencimento1parcelasinal2.date) < DataBase)
                        return "Sinal 3: Vencimento da parcela sinal é inferior a data base.";

                    //Sinal 4
                    if (oQuote.new_datadevencimento1parcelasinal3 != null && Utility.ParseDateTime(oQuote.new_datadevencimento1parcelasinal3.date) < DataBase)
                        return "Sinal 4: Vencimento da parcela sinal é inferior a data base.";
                }

                //Mensal 1
                if (oQuote.new_datadepagamentomensal != null && Utility.ParseDateTime(oQuote.new_datadepagamentomensal.date) < DataBase)
                    return "Mensal 1: Vencimento da parcela mensal é inferior a data base.";
                //Mensal 2
                if (oQuote.new_datado1pagamentomensal1 != null && Utility.ParseDateTime(oQuote.new_datado1pagamentomensal1.date) < DataBase)
                    return "Mensal 2: Vencimento da parcela mensal é inferior a data base.";
                //Mensal 3
                if (oQuote.new_datado1pagamentomensal2 != null && Utility.ParseDateTime(oQuote.new_datado1pagamentomensal2.date) < DataBase)
                    return "Mensal 3: Vencimento da parcela mensal é inferior a data base.";
                //Mensal 4
                if (oQuote.new_datado1pagamentomensal3 != null && Utility.ParseDateTime(oQuote.new_datado1pagamentomensal3.date) < DataBase)
                    return "Mensal 4: Vencimento da parcela mensal é inferior a data base.";
                //Mensal 5
                if (oQuote.new_datado1pagamentomensal4 != null && Utility.ParseDateTime(oQuote.new_datado1pagamentomensal4.date) < DataBase)
                    return "Mensal 5: Vencimento da parcela mensal é inferior a data base.";
                //Mensal 6
                if (oQuote.new_datado1pagamentomensal5 != null && Utility.ParseDateTime(oQuote.new_datado1pagamentomensal5.date) < DataBase)
                    return "Mensal 6: Vencimento da parcela mensal é inferior a data base.";

                //Intermediaria 1
                if (oQuote.new_datadepagamentointermediaria != null && Utility.ParseDateTime(oQuote.new_datadepagamentointermediaria.date) < DataBase)
                    return "Intermediária 1: Vencimento da parcela intermediária é inferior a data base.";
                //Intermediaria 2
                if (oQuote.new_datadepagamentointermediria1 != null && Utility.ParseDateTime(oQuote.new_datadepagamentointermediria1.date) < DataBase)
                    return "Intermediária 1: Vencimento da parcela intermediária é inferior a data base.";
                //Intermediaria 3
                if (oQuote.new_datadepagamentointermediria2 != null && Utility.ParseDateTime(oQuote.new_datadepagamentointermediria2.date) < DataBase)
                    return "Intermediária 3: Vencimento da parcela intermediária é inferior a data base.";

                Mrv.Crm.ValueObjects.ParametroComercialValueObject usarDataLimite = this.DominioParametroComercial.UsarDataLimite();
                if (usarDataLimite.UsarDataLimite)
                {
                    if (DateTime.Today > usarDataLimite.DataLimite)//Se hoje maior que a data limite
                        return "Data inválida.\nData permitida: Apenas dia 5 a partir de " + oCulture.TextInfo.ToTitleCase(dtFormatInfo.GetMonthName(DateTime.Today.Month));
                    else
                    {
                        if (DataBase.Day != 5)
                            return "Data inválida.\nData permitida: Apenas dia 5 a partir de " + oCulture.TextInfo.ToTitleCase(dtFormatInfo.GetMonthName(usarDataLimite.DataLimite.Month - 1));

                        if (DataBase.Month >= dtHoje.Month)
                            return true.ToString().ToLower();

                        if (DataBase.Month != usarDataLimite.DataLimite.Month - 1)
                            return "Data inválida.\nData permitida: Apenas dia 5 a partir de " + oCulture.TextInfo.ToTitleCase(dtFormatInfo.GetMonthName(usarDataLimite.DataLimite.Month - 1));

                        return true.ToString().ToLower();
                    }
                }


                if (DataBase < dtHoje)
                    return "Data inferior a permitida.\nData permitida: Apenas dia 5 a partir de " + oCulture.TextInfo.ToTitleCase(dtFormatInfo.GetMonthName(dtHoje.Month));
                else if (DataBase.Day != 5)
                    return "Data permitida: Apenas dia 5 a partir de " + oCulture.TextInfo.ToTitleCase(dtFormatInfo.GetMonthName(dtHoje.Month));
                else
                    return "true";
            }
            else
                return "Oportunidade relacionada a este contrato não possui proposta ganha.";
        }

        public string ValidarPreenchimentoTaxaDespachante(quote proposta)
        {
            //quote proposta = this.ObterPorId(idProposta) as quote;
            PlanoFinanciamento plano = new PlanoFinanciamento();
            if (proposta.new_tipodecotacao.Value == (int)TipoVenda.Unidade)
            {
                PlanoFinanciamentoValueObject informacoesPlano = plano.ObterInformacoesPlanoFinanciamento(proposta.opportunityid.Value);
                if (informacoesPlano != null)
                {
                    string nomePlano = informacoesPlano.Nome.ToUpper();

                    bool taxaObrigatoriaPorCidade = informacoesPlano.TaxaObrigatoriaPorCidade;
                    bool taxaObrigatoriaPorPlano = informacoesPlano.TaxaObrigatoriaPorPlano;

                    if (taxaObrigatoriaPorCidade && taxaObrigatoriaPorPlano)
                    {
                        if (proposta.new_forma_pagamento_tx_desp == null && proposta.new_justificativa_taxa_despachante == null)
                        {
                            return Mensagem.MENSAGEM_FORMA_PAGAMENTO_JUSTIFICATIVA_NULO;
                        }
                        else if (proposta.new_forma_pagamento_tx_desp != null && proposta.new_justificativa_taxa_despachante != null)
                        {
                            return Mensagem.MENSAGEM_FORMA_PAGAMENTO_JUSTIFICATIVA_NAO_NULO;
                        }
                        else
                        {
                            if (proposta.new_forma_pagamento_tx_desp != null)
                            {
                                if (proposta.new_vlr_tx == null || proposta.new_forma_pgto_taxa_interna == null || proposta.new_parcelamentotaxasinterno == null)
                                {
                                    return Mensagem.MENSAGEM_VALOR_TAXA_TIPO_PAGAMENTO_PARCELAMENTO_TAXA_NULO;
                                }
                            }
                            else
                            {
                                if (proposta.new_vlr_tx != null || proposta.new_forma_pgto_taxa_interna != null || proposta.new_parcelamentotaxasinterno != null)
                                {
                                    return Mensagem.MENSAGEM_VALOR_TAXA_TIPO_PAGAMENTO_PARCELAMENTO_TAXA_NAO_NULO;
                                }
                            }
                        }
                    }
                }
            }

            return string.Empty;
        }
        public bool ValidarDataReserva(quote proposta)
        {
            if (IsProductSale(proposta.new_tipodecotacao.Value))
            {
                return
                proposta.new_inicio_reserva.IsNull ||
                proposta.new_fim_ativacao.IsNull ||
                proposta.new_fim_negociacao.IsNull ||
                proposta.new_fim_reserva.IsNull;
            }
            else
            {
                return false;
            }
        }

        public bool IsProductSale(int proposalType)
        {
            return proposalType == (int)TipoVenda.Unidade ||
                proposalType == (int)TipoVenda.Garagem ||
                proposalType == (int)TipoVenda.Lote ||
                proposalType == (int)TipoVenda.Loja;
        }

        public string ValidarAnaliseCredito(Guid idProposta, bool contraProposta)
        {
            var proposta = ObterPorId(idProposta) as quote;
            var mensagemRetorno = new StringBuilder();
            var retorno = ValidarAnaliseCredito(proposta, contraProposta);
            if (retorno != null && retorno.Count > 0)
            {
                foreach (var mensagem in retorno)
                {
                    mensagemRetorno.AppendLine(mensagem);
                }
            }
            return mensagemRetorno.ToString();
        }

        public List<RetornoPadrao> ValidarAnaliseCreditoMrvCredito(Guid idProposta,bool contraProposta)
        {
            var proposta = ObterPorId(idProposta) as quote;
            var retorno = new List<RetornoPadrao>();
            var mensagensValidacao = ValidarAnaliseCredito(proposta,contraProposta);
            if (mensagensValidacao.Count > 0)
            {
                mensagensValidacao.ForEach(p => retorno.Add(new RetornoPadrao(p)));
                return retorno;
            }
            else
            {
                var retornoRegrasReutilizacaoPac = ValidarRegraReutilizacaoPAC(proposta);
                if (!string.IsNullOrEmpty(retornoRegrasReutilizacaoPac))
                {
                    var retornoPadraoReutilizacao = new RetornoPadrao();
                    retornoPadraoReutilizacao.Mensagem = retornoRegrasReutilizacaoPac;
                    retorno.Add(retornoPadraoReutilizacao);
                    return retorno;
                }
            }

            return retorno;
        }

        public List<string> ValidarAnaliseCredito(quote proposta, bool contraProposta)
        {
            var mensagemValidacao = new List<string>();
            //obtem oportunidade
            var oportunidade = DominioOportunidade.ObterPorId(proposta.opportunityid.Value) as opportunity;
            //valida de a oportunidade é de venda de unidade para validar taxa despachante
            if (oportunidade != null && oportunidade.new_tipodeoportunidade.Value == Oportunidade.TIPO_VENDA_UNIDADE)
            {
                //Obtem empreendimento
                var empreendimento = DominioEmpreendimento.ObterPorOportunidade(proposta.opportunityid.Value, "new_cidadeid");
                //Obtem Cidade para validação de Taxa de Registro
                var cidade = DominioCidade.ObterPoId(empreendimento.new_cidadeid.Value);
                if (cidade == null)
                {
                    mensagemValidacao.Add(Mensagem.CIDADE_NAO_DEFINIDA);
                    return mensagemValidacao;
                }

                RepositorioPropostaAnaliseCredito.UseAllFields = true;
                var propostaAnaliseCreditoCrm = RepositorioPropostaAnaliseCredito.ObterPorOportunidade(proposta.opportunityid.Value);

                var mensagem = ValidarRendaPropostaRendaPac(proposta, propostaAnaliseCreditoCrm, Utility.MomentoValidacaoPAC.GanharProposta);
                if (!string.IsNullOrEmpty(mensagem))
                {
                    mensagemValidacao.Add(mensagem);
                }

                //Regra do benefício ISS
                var retornoIss = DominioOportunidade.ValidarRendaLimiteBeneficioIss(oportunidade.opportunityid.Value);
                if (!retornoIss.ValorRendaMenor)
                {
                    mensagemValidacao.Add(retornoIss.Mensagem);
                }


                bool cidadeProcessoSicaqSacBb = cidade.new_processo_sicaq_sacbb != null && cidade.new_processo_sicaq_sacbb.Value;
                if (cidadeProcessoSicaqSacBb)
                {
                    //verifica se a oportunidade não foi criada pelo MRV Corretor.
                    //Troca da verificação de new_corretoroportunidadeid por new_loja_criacaoid
                    if (oportunidade.new_loja_criacaoid == null)
                    {
                        return mensagemValidacao;
                    }
                    //Caso não exista PAC o sistema verificará se a loja do corretor utiliza o módulo do MRV Crédito,
                    //Se utilizar sitema informa que será ncessário solicitar uma PAC para pré ativar o contrato.
                    if (propostaAnaliseCreditoCrm == null)
                    {
                        //Verificar se o Plano de financimento da Oportunidade
                        //exige análise de crédito
                        new_planodefinanciamento planoFinanciamento = new PlanoFinanciamento().ObterPorOportunidade(proposta.opportunityid.Value);
                        if (planoFinanciamento == null)
                        {
                            mensagemValidacao.Add(Mensagem.PROPOSTA_ANALISE_CREDITO_PLANO_FINACIAMENTO_INEXISTENTE);
                        }
                        //Valida se o plano de finaciamento exige uma análise de crédito
                        if (planoFinanciamento != null && (planoFinanciamento.new_solicita_analise_credito == null ||
                                                           planoFinanciamento.new_solicita_analise_credito.Value == false))
                        {
                            return mensagemValidacao;
                        }

                        //Se nao existe PAC
                        //Sera verificado se a data de criação da proposta é menor do que a data de inclusão da loja ao novo processo do crédito.
                        RepositorioLoja.UseAllFields = true;
                        var lojaCrm = RepositorioLoja.Obter(oportunidade.new_loja_criacaoid.Value);
                        if (lojaCrm != null)
                        {
                            //verifica se a loja esta habilitada a usar o módulo de
                            //crédito
                            if (lojaCrm.new_usamrvcredito != null && lojaCrm.new_usamrvcredito.Value)
                            {
                                if (lojaCrm.new_data_inclusao_credito != null)
                                {
                                    //verifica se a criação da proposta é maior ou igual a data de inclusão
                                    //e a loja esta habilitada, caso seja verdadeira será necessário a criação da pac.
                                    if (Convert.ToDateTime(proposta.createdon.Value) >= Convert.ToDateTime(lojaCrm.new_data_inclusao_credito.Value) &&
                                        lojaCrm.new_usamrvcredito.Value)
                                    {
                                        mensagemValidacao.Add(Mensagem.PROPOSTA_ANALISE_CREDITO_INEXISTENTE);
                                        return mensagemValidacao;
                                    }
                                }
                                else
                                {
                                    mensagemValidacao.Add(Mensagem.DATA_INCLUSAO_LOJA_NULA);
                                }
                            }
                        }
                    }
                    //Verificar se existe solicitação de análise de crédito.
                    if (propostaAnaliseCreditoCrm != null)
                    {
                        //valida se é análise de crédito.
                        if (propostaAnaliseCreditoCrm.new_propostaanalisecreditoid != null)
                        {
                            if (propostaAnaliseCreditoCrm.new_tiposolicitacao.Value == (int)Entidade.PropostaAnaliseCredito.TipoSolicitacao.ANALISE_DE_CREDITO)
                            {
                                if (propostaAnaliseCreditoCrm.new_statusdaanalise != null)
                                {
                                    var retornoStatusPac = DominioPropostaAnaliseCredito.ValidarStatusPAC(propostaAnaliseCreditoCrm.new_statusdaanalise.Value, Utility.MomentoValidacaoPAC.GanharProposta);
                                    if (!string.IsNullOrEmpty(retornoStatusPac))
                                    {
                                        mensagemValidacao.Add(retornoStatusPac);
                                        return mensagemValidacao;
                                    }
                                }
                            }
                        }
                    }
                }

                var classificacaoPlanoFinanciamento = DominioPropostaVenda.ObterTipoPlanoFinanciamento(oportunidade);

                if (Entidade.PlanoDeFinanciamento.Classificacao.FLEX != classificacaoPlanoFinanciamento)
                {
                    var retornoEnquadramentoPAC = ValidarRegraEnquadramentoPAC(propostaAnaliseCreditoCrm, proposta, oportunidade, Utility.MomentoValidacaoPAC.GanharProposta);
                    if (!string.IsNullOrEmpty(retornoEnquadramentoPAC))
                    {
                        mensagemValidacao.Add(retornoEnquadramentoPAC);
                    }

                    var retornoValoresFinanciamentoPropostaMenorPac = ValidarValoresFinanciamentoPropostaMenorPac(propostaAnaliseCreditoCrm, proposta);
                    if (!string.IsNullOrEmpty(retornoValoresFinanciamentoPropostaMenorPac))
                    {
                        mensagemValidacao.Add(retornoValoresFinanciamentoPropostaMenorPac);
                    }

                    var retornoValidarValoresFGTSPropostaPac = ValidarValoresFGTSPropostaPac(propostaAnaliseCreditoCrm, proposta);
                    if (!string.IsNullOrEmpty(retornoValidarValoresFGTSPropostaPac))
                    {
                        mensagemValidacao.Add(retornoValidarValoresFGTSPropostaPac);
                    }

                    var retornoValidarStatusPropostaPac = ValidarStatusPropostaPac(propostaAnaliseCreditoCrm, proposta);
                    if (!string.IsNullOrEmpty(retornoValidarStatusPropostaPac))
                    {
                        mensagemValidacao.Add(retornoValidarStatusPropostaPac);
                    }

                    var retornoValidarValoresFinanciamentoPropostaMaiorPac = ValidarValoresFinanciamentoPropostaMaiorPac(propostaAnaliseCreditoCrm, proposta, Utility.MomentoValidacaoPAC.GanharProposta);
                    if (!string.IsNullOrEmpty(retornoValidarValoresFinanciamentoPropostaMaiorPac))
                    {
                        mensagemValidacao.Add(retornoValidarValoresFinanciamentoPropostaMaiorPac);
                    }

                    var retornoValidarValoresFGTSPropostaMaiorPac = ValidarValoresFGTSPropostaMaiorPac(propostaAnaliseCreditoCrm, proposta);
                    if (!string.IsNullOrEmpty(retornoValidarValoresFGTSPropostaMaiorPac))
                    {
                        mensagemValidacao.Add(retornoValidarValoresFGTSPropostaMaiorPac);
                    }

                    var retornoValidarValorSubsidioPropostaMenorPac = ValidarValorSubsidioPropostaMenorPac(propostaAnaliseCreditoCrm, proposta);
                    if (!string.IsNullOrEmpty(retornoValidarValorSubsidioPropostaMenorPac))
                    {
                        mensagemValidacao.Add(retornoValidarValorSubsidioPropostaMenorPac);
                    }
                }
                DominioPropostaVenda.ValidarValorDescontoIndicacao(proposta, mensagemValidacao, oportunidade, propostaAnaliseCreditoCrm,contraProposta);
            }
            return mensagemValidacao;
        }


        public string ValidarRendaPropostaRendaPac(quote proposta, new_propostaanalisecredito propostaAnaliseCreditoCrm, Utility.MomentoValidacaoPAC tipoValidacao)
        {
            var mensagem = string.Empty;

            if (proposta != null && propostaAnaliseCreditoCrm != null && propostaAnaliseCreditoCrm.new_statusdaanalise != null)
            {
                mensagem = DominioPropostaAnaliseCredito.ValidarStatusPAC(propostaAnaliseCreditoCrm.new_statusdaanalise.Value, Utility.MomentoValidacaoPAC.GanharProposta);
                if (string.IsNullOrEmpty(mensagem))
                {
                    if (propostaAnaliseCreditoCrm.new_rendatotal == null)
                    {
                        propostaAnaliseCreditoCrm.new_rendatotal = new CrmDecimal() { Value = 0 };
                    }
                    var rendaTotalPac = propostaAnaliseCreditoCrm.new_rendatotal.Value;
                    if (proposta.new_renda_apurada_credito == null)
                    {
                        proposta.new_renda_apurada_credito = new CrmDecimal() { Value = 0 };
                    }
                    var rendaApuradaCredito = proposta.new_renda_apurada_credito.Value;

                    if (rendaApuradaCredito > 0)
                    {
                        if (!CompararValoresPropostaPacComDelta(rendaApuradaCredito, rendaTotalPac, Utility.TipoCalculoDelta.Renda, Utility.TipoCalculoMargem.Igual))
                        {
                            if (Utility.MomentoValidacaoPAC.GanharProposta == tipoValidacao)
                            {
                                mensagem = string.Format(Mensagem.PAC_RENDA_APURADA_ALTERADA, Mensagem.RENDA_APURADA_CREDITO_PROPOSTA, Utility.formataMoeda(rendaApuradaCredito.ToString()), Utility.formataMoeda(rendaTotalPac.ToString()));
                            }
                            else
                            {
                                mensagem = string.Format(Mensagem.PAC_RENDA_APURADA_ALTERADA_PRE_ATIVAR, Mensagem.RENDA_APURADA_CREDITO_PROPOSTA, Utility.formataMoeda(rendaApuradaCredito.ToString()), Utility.formataMoeda(rendaTotalPac.ToString()), Mensagem.RENDA_APURADA_CREDITO_PROPOSTA);
                            }
                        }
                    }
                    else
                    {
                        if (proposta.new_renda_presumida != null)
                        {
                            var rendaPresumida = proposta.new_renda_presumida.Value;
                            if (!CompararValoresPropostaPacComDelta(rendaPresumida, rendaTotalPac, Utility.TipoCalculoDelta.Renda, Utility.TipoCalculoMargem.Igual))
                            {
                                if (Utility.MomentoValidacaoPAC.GanharProposta == tipoValidacao)
                                {
                                    mensagem = string.Format(Mensagem.PAC_RENDA_APURADA_ALTERADA, Mensagem.RENDA_PRESUMIDA, Utility.formataMoeda(rendaPresumida.ToString()), Utility.formataMoeda(rendaTotalPac.ToString()));
                                }
                                else
                                {
                                    mensagem = string.Format(Mensagem.PAC_RENDA_APURADA_ALTERADA_PRE_ATIVAR, Mensagem.RENDA_PRESUMIDA, Utility.formataMoeda(rendaPresumida.ToString()), Utility.formataMoeda(rendaTotalPac.ToString()), Mensagem.RENDA_PRESUMIDA);
                                }
                            }
                        }
                    }
                }
                else
                {
                    mensagem = string.Empty;
                }
            }

            return mensagem;
        }

        public string ValidarRegraEnquadramentoPAC(new_propostaanalisecredito propostaAnaliseCreditoCRM, quote proposta, opportunity oportunidade, Utility.MomentoValidacaoPAC tipoValidacao)
        {
            var mensagem = string.Empty;
            double valorTotalProposta = 0;
            decimal valorCompraVenda = 0;

            if (propostaAnaliseCreditoCRM != null && propostaAnaliseCreditoCRM.new_statusdaanalise != null && proposta != null && oportunidade != null)
            {
                mensagem = DominioPropostaAnaliseCredito.ValidarStatusPAC(propostaAnaliseCreditoCRM.new_statusdaanalise.Value, Utility.MomentoValidacaoPAC.GanharProposta);
                if (string.IsNullOrEmpty(mensagem))
                {

                    var valorCompraVendaDivergenteValorTotalProposta = false;
                    if (propostaAnaliseCreditoCRM.new_valor_compra_venda == null)
                    {
                        propostaAnaliseCreditoCRM.new_valor_compra_venda = new CrmMoney() { Value = 0 };
                    }
                    if (propostaAnaliseCreditoCRM.new_valor_compra_venda != null && proposta.new_valortotalproposta != null)
                    {
                        valorTotalProposta = proposta.new_valortotalproposta.Value;
                        valorCompraVenda = propostaAnaliseCreditoCRM.new_valor_compra_venda.Value;
                        if (oportunidade.new_elegibilidade != null && oportunidade.new_elegibilidade.Value == Entidade.Oportunidade.Elegibilidade.SACBB)
                        {
                            valorCompraVendaDivergenteValorTotalProposta = propostaAnaliseCreditoCRM.new_valor_compra_venda.Value != Convert.ToDecimal(proposta.new_valortotalproposta.Value);
                        }
                        else
                        {
                            if (oportunidade.new_elegibilidade != null)
                            {
                                valorCompraVendaDivergenteValorTotalProposta = DiferencaMaiorUmaUnidadeValor(propostaAnaliseCreditoCRM.new_valor_compra_venda.Value, Convert.ToDecimal(proposta.new_valortotalproposta.Value));
                            }
                        }
                    }

                    if (!valorCompraVendaDivergenteValorTotalProposta)
                    {
                        return mensagem;
                    }
                    if (tipoValidacao == Utility.MomentoValidacaoPAC.GanharProposta)
                    {
                        mensagem = string.Format(Mensagem.VALOR_COMPRA_VENDA_DIVERGENTE_VALOR_PROPOSTA, valorTotalProposta, valorCompraVenda);
                    }
                    else
                    {
                        mensagem = string.Format(Mensagem.VALOR_COMPRA_VENDA_DIVERGENTE_VALOR_PROPOSTA_CONTRATO, valorTotalProposta, valorCompraVenda);
                    }
                }
                else
                {
                    mensagem = string.Empty;
                }
            }

            return mensagem;
        }

        protected bool DiferencaMaiorUmaUnidadeValor(decimal valorReferencia, decimal valorComparacao)
        {
            var valorMinimo = valorReferencia - 1;
            var valorMaximo = valorReferencia + 1;
            return valorComparacao < valorMinimo || valorComparacao > valorMaximo;
        }

        public string ValidarRegraReutilizacaoPAC(quote proposta)
        {
            new_propostaanalisecredito pac = RepositorioPropostaAnaliseCredito.ObterPorOportunidade(proposta.opportunityid.Value);
            string errosValidacao = string.Empty;
            if (pac != null && pac.new_id_oportunidade_anterior != null && pac.new_numeropac != null)
            {
                if ((pac.new_pac_reaproveitada != null && pac.new_pac_reaproveitada.Value) && DominioPropostaAnaliseCredito.ValidarPacAprovada(pac))
                {
                    opportunity oportunidadePAC = DominioOportunidade.ObterPorId(proposta.opportunityid.Value) as opportunity;
                    if (oportunidadePAC != null && oportunidadePAC.new_tipodeoportunidade.Value == Oportunidade.TIPO_VENDA_UNIDADE)
                    {
                        opportunity oportunidadeAnteriorPAC = DominioOportunidade.ObterPorId(pac.new_id_oportunidade_anterior.Value) as opportunity; //Oportunidade PAC Anterior
                        if (oportunidadeAnteriorPAC != null)
                        {
                            decimal? precoAvaliacaoEmpreendimento = null;
                            decimal? precoAvaliacaoEmpreendimentoAnterior = null;
                            decimal? valorOficialEmpreendimento = null;
                            decimal? valorOficialEmpreendimentoAnterior = null;
                            decimal? precoAvaliacaoPiloto = null;
                            decimal? precoAvaliacaoPilotoAnterior = null;
                            decimal? valorOficialPiloto = null;
                            decimal? valorOficialPilotoAnterior = null;
                            decimal? valorTotalProposta = null;
                            decimal? valorPropostaOportunidadeAnterior = null;

                            if (oportunidadePAC.new_preco_avaliacao_empreendimento != null)
                            {
                                precoAvaliacaoEmpreendimento = oportunidadePAC.new_preco_avaliacao_empreendimento.Value;
                            }
                            if (oportunidadeAnteriorPAC.new_preco_avaliacao_empreendimento != null)
                            {
                                precoAvaliacaoEmpreendimentoAnterior = oportunidadeAnteriorPAC.new_preco_avaliacao_empreendimento.Value;
                            }

                            if (oportunidadePAC.new_valor_oficial_empreendimento != null)
                            {
                                valorOficialEmpreendimento = oportunidadePAC.new_valor_oficial_empreendimento.Value;
                            }
                            if (oportunidadeAnteriorPAC.new_valor_oficial_empreendimento != null)
                            {
                                valorOficialEmpreendimentoAnterior = oportunidadeAnteriorPAC.new_valor_oficial_empreendimento.Value;
                            }

                            if (oportunidadePAC.new_preco_avaliacao_piloto != null)
                            {
                                precoAvaliacaoPiloto = oportunidadePAC.new_preco_avaliacao_piloto.Value;
                            }
                            if (oportunidadeAnteriorPAC.new_preco_avaliacao_piloto != null)
                            {
                                precoAvaliacaoPilotoAnterior = oportunidadeAnteriorPAC.new_preco_avaliacao_piloto.Value;
                            }

                            if (oportunidadePAC.new_valor_oficial_piloto != null)
                            {
                                valorOficialPiloto = oportunidadePAC.new_valor_oficial_piloto.Value;
                            }
                            if (oportunidadeAnteriorPAC.new_valor_oficial_piloto != null)
                            {
                                valorOficialPilotoAnterior = oportunidadeAnteriorPAC.new_valor_oficial_piloto.Value;
                            }

                            if (proposta.new_valortotalproposta != null)
                            {
                                valorTotalProposta = (decimal)proposta.new_valortotalproposta.Value;
                            }
                            if (oportunidadeAnteriorPAC.new_valor_ganho_proposta != null)
                            {
                                valorPropostaOportunidadeAnterior = oportunidadeAnteriorPAC.new_valor_ganho_proposta.Value;
                            }

                            if (precoAvaliacaoEmpreendimento != precoAvaliacaoEmpreendimentoAnterior ||
                                        valorOficialEmpreendimento != valorOficialEmpreendimentoAnterior ||
                                        precoAvaliacaoPiloto != precoAvaliacaoPilotoAnterior ||
                                        valorOficialPiloto != valorOficialPilotoAnterior)
                            {
                                errosValidacao = String.Format(Mensagem.VALOR_AVALIACAO_DIFERENTE,
                                    precoAvaliacaoPiloto.HasValue ? precoAvaliacaoPiloto : 0,
                                    precoAvaliacaoPilotoAnterior.HasValue ? precoAvaliacaoPilotoAnterior : 0);
                            }
                            if (valorTotalProposta != valorPropostaOportunidadeAnterior)
                            {
                                errosValidacao = String.Format(Mensagem.VALOR_TOTAL_PROPOSTA_DIFERENTE,
                                    valorTotalProposta.HasValue ? valorTotalProposta : 0,
                                    valorPropostaOportunidadeAnterior.HasValue ? valorPropostaOportunidadeAnterior : 0);
                            }
                            bool alteraStatus = !string.IsNullOrEmpty(errosValidacao);
                            string mensagem = alteraStatus ? Mensagem.PAC_EM_DISCUSSAO_REUTILIZADA : Mensagem.PAC_APROVADA_REUTILIZADA;

                            new_propostaanalisecredito pacUpdate = new new_propostaanalisecredito();
                            pacUpdate.new_propostaanalisecreditoid = new Key() { Value = pac.new_propostaanalisecreditoid.Value };
                            pacUpdate.new_pac_reaproveitada = new CrmBoolean() { Value = true };

                            if (alteraStatus)
                            {
                                pacUpdate.new_statusdaanalise = new Picklist() { Value = Entidade.PropostaAnaliseCredito.StatusPropostaAnaliseCreditoMRVCorretor.Em_Discussao };
                            }
                            RepositorioPropostaAnaliseCredito.Atualizar(pacUpdate);

                            DominioSinergia.GravarHistoricoAtividadePAC(pac.new_numeropac.Value, Entidade.HistoricoAtividade.SistemaOrigem.CRM, mensagem);
                        }
                    }
                }
            }
            return errosValidacao;
        }

        public virtual void SolicitarAprovacao(Guid idProposta) { }

        public RetornoRegraParcelasDesconto ValidarRegrasParcelaDesconto(ParcelaDescontoValueObject requisicao)
        {
            return RepositorioMotorRegrasProposta.ValidacaoRegraParcelasDesconto(requisicao);
        }

        #region Validações Financiamento/fgts minimo

        public string ValidarValoresFinanciamentoPropostaMenorPac(new_propostaanalisecredito propostaAnaliseCreditoCRM, quote proposta)
        {
            var mensagem = string.Empty;
            if (propostaAnaliseCreditoCRM != null && propostaAnaliseCreditoCRM.new_statusdaanalise != null && proposta != null && proposta.new_status_solicitacao_pac != null)
            {
                var statusPac = DominioPropostaAnaliseCredito.ValidarStatusPAC(propostaAnaliseCreditoCRM.new_statusdaanalise.Value, Utility.MomentoValidacaoPAC.GanharProposta);
                var statusProposta = DominioPropostaAnaliseCredito.ValidarStatusPAC(proposta.new_status_solicitacao_pac.Value, Utility.MomentoValidacaoPAC.GanharProposta);
                if (string.IsNullOrEmpty(statusPac) && string.IsNullOrEmpty(statusProposta))
                {
                    if (proposta.new_valor_financiamento_sac == null)
                    {
                        proposta.new_valor_financiamento_sac = new CrmDecimal { Value = 0 };
                    }
                    var valorFinanciamentoSacProposta = proposta.new_valor_financiamento_sac.Value;

                    if (proposta.new_valor_financiamento_price == null)
                    {
                        proposta.new_valor_financiamento_price = new CrmDecimal { Value = 0 };
                    }
                    var valorFinanciamentoPriceProposta = proposta.new_valor_financiamento_price.Value;

                    if (propostaAnaliseCreditoCRM.new_valorfinanciamento == null)
                    {
                        propostaAnaliseCreditoCRM.new_valorfinanciamento = new CrmDecimal { Value = 0 };
                    }
                    var valorFinanciamentoSacPropostaAnaliseCredito = propostaAnaliseCreditoCRM.new_valorfinanciamento.Value;

                    if (propostaAnaliseCreditoCRM.new_valor_financiamento_price == null)
                    {
                        propostaAnaliseCreditoCRM.new_valor_financiamento_price = new CrmDecimal { Value = 0 };
                    }
                    var valorFinanciamentoPricePropostaAnaliseCredito = propostaAnaliseCreditoCRM.new_valor_financiamento_price.Value;

                    var maiorValorProposta = valorFinanciamentoSacProposta;
                    if (maiorValorProposta < valorFinanciamentoPriceProposta)
                    {
                        maiorValorProposta = valorFinanciamentoPriceProposta;
                    }

                    var maiorValorPac = valorFinanciamentoSacPropostaAnaliseCredito;

                    if (maiorValorPac < valorFinanciamentoPricePropostaAnaliseCredito)
                    {
                        maiorValorPac = valorFinanciamentoPricePropostaAnaliseCredito;
                    }

                    var propostaMenorPropostaAnaliseCredito = CompararValoresPropostaPacComDelta(maiorValorProposta, maiorValorPac, Utility.TipoCalculoDelta.Financiamento, Utility.TipoCalculoMargem.Inferior);

                    if (propostaMenorPropostaAnaliseCredito)
                    {
                        mensagem = string.Format(Mensagem.VALORES_FINANCIAMENTO_PROPOSTA_MENOR_VALORES_FINANCIAMENTO_PAC, Utility.formataMoeda(valorFinanciamentoSacProposta.ToString()), Utility.formataMoeda(valorFinanciamentoPriceProposta.ToString()),
                            Utility.formataMoeda(valorFinanciamentoSacPropostaAnaliseCredito.ToString()), Utility.formataMoeda(valorFinanciamentoPricePropostaAnaliseCredito.ToString()));
                    }
                }
            }

            return mensagem;
        }

        public string ValidarValoresFGTSPropostaPac(new_propostaanalisecredito propostaAnaliseCreditoCRM, quote proposta)
        {
            var mensagem = string.Empty;
            if (propostaAnaliseCreditoCRM != null && propostaAnaliseCreditoCRM.new_statusdaanalise != null && proposta != null && proposta.new_status_solicitacao_pac != null)
            {
                var statusPac = DominioPropostaAnaliseCredito.ValidarStatusPAC(propostaAnaliseCreditoCRM.new_statusdaanalise.Value, Utility.MomentoValidacaoPAC.GanharProposta);
                var statusProposta = DominioPropostaAnaliseCredito.ValidarStatusPAC(proposta.new_status_solicitacao_pac.Value, Utility.MomentoValidacaoPAC.GanharProposta);
                if (string.IsNullOrEmpty(statusPac) && string.IsNullOrEmpty(statusProposta))
                {
                    if (proposta.new_valor_fgts_pac == null)
                    {
                        proposta.new_valor_fgts_pac = new CrmDecimal() { Value = 0 };
                    }
                    var valorFGTSProposta = proposta.new_valor_fgts_pac.Value;

                    if (propostaAnaliseCreditoCRM.new_valorfgts == null)
                    {
                        propostaAnaliseCreditoCRM.new_valorfgts = new CrmDecimal() { Value = 0 };
                    }
                    var valorFGTSPropostaAnaliseCredito = propostaAnaliseCreditoCRM.new_valorfgts.Value;

                    var propostaMenorPropostaAnaliseCredito = CompararValoresPropostaPacComDelta(valorFGTSProposta, valorFGTSPropostaAnaliseCredito, Utility.TipoCalculoDelta.FGTS, Utility.TipoCalculoMargem.Inferior); //valorFGTSProposta < valorFGTSPropostaAnaliseCredito;

                    if (propostaMenorPropostaAnaliseCredito)
                    {
                        mensagem = string.Format(Mensagem.VALORES_FGTS_PROPOSTA_MENOR_VALORES_FINANCIAMENTO_PAC, Utility.formataMoeda(valorFGTSProposta.ToString()), Utility.formataMoeda(valorFGTSPropostaAnaliseCredito.ToString()));
                    }
                }
            }

            return mensagem;
        }

        public string ValidarStatusPropostaPac(new_propostaanalisecredito propostaAnaliseCreditoCRM, quote proposta)
        {
            var mensagem = string.Empty;
            if (propostaAnaliseCreditoCRM != null && propostaAnaliseCreditoCRM.new_statusdaanalise != null && proposta != null)
            {
                var status = DominioPropostaAnaliseCredito.ValidarStatusPAC(propostaAnaliseCreditoCRM.new_statusdaanalise.Value, Utility.MomentoValidacaoPAC.GanharProposta);
                if (!string.IsNullOrEmpty(status) && proposta.new_status_solicitacao_pac != null)
                {
                    var statusAnalise = proposta.new_status_solicitacao_pac.Value;

                    if (CodigosStatusPacAprovadas.Contains(statusAnalise))
                    {
                        mensagem = string.Format(Mensagem.VALORES_STATUS_PROPOSTA_DIFERENTE_PAC, propostaAnaliseCreditoCRM.new_statusdaanalise.name, proposta.new_status_solicitacao_pac.name);
                    }
                }
            }
            return mensagem;
        }

        public string ValidarValoresFinanciamentoPropostaMaiorPac(new_propostaanalisecredito propostaAnaliseCreditoCrm, quote proposta, Utility.MomentoValidacaoPAC momentoValidacao)
        {
            var mensagem = string.Empty;
            if (propostaAnaliseCreditoCrm != null && propostaAnaliseCreditoCrm.new_statusdaanalise != null && proposta != null)
            {
                var status = DominioPropostaAnaliseCredito.ValidarStatusPAC(propostaAnaliseCreditoCrm.new_statusdaanalise.Value, momentoValidacao);
                if (string.IsNullOrEmpty(status))
                {
                    if (proposta.new_financiamento == null)
                    {
                        proposta.new_financiamento = new CrmFloat { Value = 0 };
                    }
                    var valorFinanciamentoProposta = proposta.new_financiamento.Value;

                    if (propostaAnaliseCreditoCrm.new_valorfinanciamento == null)
                    {
                        propostaAnaliseCreditoCrm.new_valorfinanciamento = new CrmDecimal { Value = 0 };
                    }
                    var valorFinanciamentoSacPropostaAnaliseCredito = propostaAnaliseCreditoCrm.new_valorfinanciamento.Value;

                    if (propostaAnaliseCreditoCrm.new_valor_financiamento_price == null)
                    {
                        propostaAnaliseCreditoCrm.new_valor_financiamento_price = new CrmDecimal { Value = 0 };
                    }
                    var valorFinanciamentoPricePropostaAnaliseCredito = propostaAnaliseCreditoCrm.new_valor_financiamento_price.Value;

                    if (propostaAnaliseCreditoCrm.new_valorsubsidio == null)
                    {
                        propostaAnaliseCreditoCrm.new_valorsubsidio = new CrmDecimal { Value = 0 };
                    }
                    var valorSubsidio = propostaAnaliseCreditoCrm.new_valorsubsidio.Value;

                    var maiorValor = valorFinanciamentoSacPropostaAnaliseCredito;

                    if (maiorValor < valorFinanciamentoPricePropostaAnaliseCredito)
                    {
                        maiorValor = valorFinanciamentoPricePropostaAnaliseCredito;
                    }

                    var maiorValorMaisSubsidio = maiorValor + valorSubsidio;

                    var propostaMaiorPropostaAnaliseCredito = CompararValoresPropostaPacComDelta(Convert.ToDecimal(valorFinanciamentoProposta), maiorValorMaisSubsidio, Utility.TipoCalculoDelta.Financiamento, Utility.TipoCalculoMargem.Superior); //valorFinanciamentoProposta > Conversao.ToDouble(maiorValorMaisSubsidio);

                    if (propostaMaiorPropostaAnaliseCredito)
                    {
                        mensagem = string.Format(Mensagem.VALORES_FINANCIAMENTO_PROPOSTA_MAIOR_VALORES_FINANCIAMENTO_PAC, Utility.formataMoeda(valorFinanciamentoProposta.ToString()), Utility.formataMoeda(maiorValor.ToString()), Utility.formataMoeda(valorSubsidio.ToString()));
                    }
                }
            }

            return mensagem;
        }

        public string ValidarValoresFGTSPropostaMaiorPac(new_propostaanalisecredito propostaAnaliseCreditoCRM, quote proposta)
        {
            var mensagem = string.Empty;
            if (propostaAnaliseCreditoCRM != null && propostaAnaliseCreditoCRM.new_statusdaanalise != null && proposta != null)
            {
                var status = DominioPropostaAnaliseCredito.ValidarStatusPAC(propostaAnaliseCreditoCRM.new_statusdaanalise.Value, Utility.MomentoValidacaoPAC.GanharProposta);
                if (string.IsNullOrEmpty(status))
                {
                    if (proposta.new_valordofgts == null)
                    {
                        proposta.new_valordofgts = new CrmMoney() { Value = 0 };
                    }
                    var valorFGTSProposta = proposta.new_valordofgts.Value;

                    if (propostaAnaliseCreditoCRM.new_valorfgts == null)
                    {
                        propostaAnaliseCreditoCRM.new_valorfgts = new CrmDecimal() { Value = 0 };
                    }
                    var valorFGTSPropostaAnaliseCredito = propostaAnaliseCreditoCRM.new_valorfgts.Value;

                    var propostaMenorPropostaAnaliseCredito = CompararValoresPropostaPacComDelta(valorFGTSProposta, valorFGTSPropostaAnaliseCredito, Utility.TipoCalculoDelta.FGTS, Utility.TipoCalculoMargem.Superior);

                    if (propostaMenorPropostaAnaliseCredito)
                    {
                        mensagem = string.Format(Mensagem.VALORES_FGTS_PROPOSTA_MAIOR_VALORES_FINANCIAMENTO_PAC, Utility.formataMoeda(valorFGTSProposta.ToString()), Utility.formataMoeda(valorFGTSPropostaAnaliseCredito.ToString()));
                    }
                }
            }

            return mensagem;
        }

        public string ValidarValorSubsidioPropostaMenorPac(new_propostaanalisecredito propostaAnaliseCreditoCRM, quote proposta)
        {
            var mensagem = string.Empty;
            if (propostaAnaliseCreditoCRM != null && propostaAnaliseCreditoCRM.new_statusdaanalise != null && proposta != null && proposta.new_status_solicitacao_pac != null)
            {
                var statusPac = DominioPropostaAnaliseCredito.ValidarStatusPAC(propostaAnaliseCreditoCRM.new_statusdaanalise.Value, Utility.MomentoValidacaoPAC.GanharProposta);
                var statusProposta = DominioPropostaAnaliseCredito.ValidarStatusPAC(proposta.new_status_solicitacao_pac.Value, Utility.MomentoValidacaoPAC.GanharProposta);
                if (string.IsNullOrEmpty(statusPac) && string.IsNullOrEmpty(statusProposta))
                {
                    if (proposta.new_valor_subsidio == null)
                    {
                        proposta.new_valor_subsidio = new CrmDecimal() { Value = 0 };
                    }
                    var valorSubsidioProposta = proposta.new_valor_subsidio.Value;

                    if (propostaAnaliseCreditoCRM.new_valorsubsidio == null)
                    {
                        propostaAnaliseCreditoCRM.new_valorsubsidio = new CrmDecimal() { Value = 0 };
                    }
                    var valorSubsidioPac = propostaAnaliseCreditoCRM.new_valorsubsidio.Value;

                    var propostaMenorPropostaAnaliseCredito = valorSubsidioProposta < valorSubsidioPac;

                    if (propostaMenorPropostaAnaliseCredito)
                    {
                        mensagem = string.Format(Mensagem.VALOR_SUBSIDIO_PROPOSTA_MENOR_VALOR_SUBSIDIO_PAC, Utility.formataMoeda(valorSubsidioProposta.ToString()), Utility.formataMoeda(valorSubsidioPac.ToString()));
                    }
                }
            }
            return mensagem;
        }

        public bool CompararValoresPropostaPacComDelta(decimal valorProposta, decimal valorPac,
          Utility.TipoCalculoDelta tipoCalculo, Utility.TipoCalculoMargem margem)
        {

            var parametroComercial = DominioParametroComercial.Obter();

            if (parametroComercial != null)
            {
                int tipoValorMais;
                int tipoValorMenos;
                decimal valorPraMais;
                decimal valorPraMenos;
                switch (tipoCalculo)
                {
                    case Utility.TipoCalculoDelta.FGTS:
                        tipoValorMais = parametroComercial.new_tipo_delta_fgts_cima != null
                            ? parametroComercial.new_tipo_delta_fgts_cima.Value
                            : 1;
                        tipoValorMenos = parametroComercial.new_tipo_delta_fgts_baixo != null
                            ? parametroComercial.new_tipo_delta_fgts_baixo.Value
                            : 1;
                        valorPraMais = parametroComercial.new_valor_delta_fgts_cima != null
                            ? parametroComercial.new_valor_delta_fgts_cima.Value
                            : 0;
                        valorPraMenos = parametroComercial.new_valor_delta_fgts_baixo != null
                            ? parametroComercial.new_valor_delta_fgts_baixo.Value
                            : 0;
                        break;
                    case Utility.TipoCalculoDelta.Financiamento:
                        tipoValorMais = parametroComercial.new_tipo_delta_financ_cima != null
                            ? parametroComercial.new_tipo_delta_financ_cima.Value
                            : 1;
                        tipoValorMenos = parametroComercial.new_tipo_delta_financ_baixo != null
                            ? parametroComercial.new_tipo_delta_financ_baixo.Value
                            : 1;
                        valorPraMais = parametroComercial.new_valor_delta_financ_cima != null
                            ? parametroComercial.new_valor_delta_financ_cima.Value
                            : 0;
                        valorPraMenos = parametroComercial.new_valor_delta_financ_baixo != null
                            ? parametroComercial.new_valor_delta_financ_baixo.Value
                            : 0;
                        break;
                    case Utility.TipoCalculoDelta.Renda:
                        tipoValorMais = parametroComercial.new_tipo_delta_renda_cima != null
                            ? parametroComercial.new_tipo_delta_renda_cima.Value
                            : 1;
                        tipoValorMenos = parametroComercial.new_tipo_delta_renda_baixo != null
                            ? parametroComercial.new_tipo_delta_renda_baixo.Value
                            : 1;
                        valorPraMais = parametroComercial.new_valor_delta_renda_cima != null
                            ? parametroComercial.new_valor_delta_renda_cima.Value
                            : 0;
                        valorPraMenos = parametroComercial.new_valor_delta_renda_baixo != null
                            ? parametroComercial.new_valor_delta_renda_baixo.Value
                            : 0;
                        break;
                    default:
                        throw new Exception(Mensagem.TIPO_CALCULO_INVALIDO);
                }

                decimal valorLimitePraMais;
                decimal valorLimitePraMenos;
                if (tipoValorMais == Entidade.ParametroComercial.TipoValorDelta.MONETARIO)
                {
                    valorLimitePraMais = valorPac + valorPraMais;

                }
                else
                {
                    valorLimitePraMais = valorPac + (valorPac * (valorPraMais / 100));

                }

                if (tipoValorMenos == Entidade.ParametroComercial.TipoValorDelta.MONETARIO)
                {
                    valorLimitePraMenos = valorPac - valorPraMenos;

                }
                else
                {
                    valorLimitePraMenos = valorPac - (valorPac * (valorPraMenos / 100));
                }

                switch (margem)
                {
                    case Utility.TipoCalculoMargem.Igual:
                        return valorProposta <= valorLimitePraMais && valorProposta >= valorLimitePraMenos;
                    case Utility.TipoCalculoMargem.Inferior:
                        return valorProposta < valorLimitePraMenos;
                    case Utility.TipoCalculoMargem.Superior:
                        return valorProposta > valorLimitePraMais;
                    default:
                        throw new Exception(Mensagem.TIPO_MARGEM_INVALIDO);
                }
            }
            else
            {
                throw new Exception(Mensagem.ERRO_OBTER_PARAMETROS_COMERCIAL);
            }
        }

        #endregion
    }
}
